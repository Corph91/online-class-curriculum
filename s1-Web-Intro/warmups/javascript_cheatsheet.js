/*
 * Let's take a look at JavaScript! This exercise is designed to make you familiar
 * with the fundamentals of the language, so take time and ask questions! We'll
 * be building a JavaScript application that help DataMessCo sort through
 * and store a bunch of their data efficiently.
 */

 // First, we need to store DataMessCo's somewhere! They have user names, rank,
 // and a "boolean" (true/false) representing whether or not that user has paid
 // their month's dues. We can store values in JavaScript by creating variables.
 // Think of variables as pointers to values. Much like a reference code for a
 // book, the name and type of a variable dictate how you find/recall it and
 // what you can do with it. In JavaScript, you can create a variable as below:
 /*
  *   var myFirst = ...;
  */
// The important thing to remember with variables is that the left hand side is
// the DECLARATION and the right hand side is the VALUE. An OPERATOR separates
// these two sides. In this case, we're using the ASSIGNMENT OPERATOR, the
// equals:
/*
 *    =
 */
// When you create a variable in JavaScript, you must always assign it an
// initial value before using it elsewhere in the program. Not doing this won't
// cause an error right away, but down-the-line it can wreak havoc in a
// program, leading to hours of debugging and wasted time. Go ahead down
// below and create three JavaScript variables like below:
 /*
  *  var name = "Luke Skywalker";
  *  var rank = 2;
  *  var paid = true;
  */
//

  var name = "Luke Skywalker";
  var rank = 2;
  var paid = true;

  var myVar = 9;
  console.log(myVar);
  console.log(myVar);
  console.log(myVar);

// Note the placement of the semicolon at the end of each variable. This isn't
// required by JavaScript, but helps denote the end of the variable
// initialization statemet!
//
// We have three separate variables, each with a different type of data stored.
// the variable "name" contains a string. String type variables are usually
// text. The next is rank, containing a Number data-type. Numbers are likewise
// tranparent in their usage in that they contain some sort of digit or
// numerical value. The final variable (paid) is a "boolean" type. Booleans
// are in almost every programming language, and represent a truth value of
// either true (1) or false (0). Boolean variables are usually used to control
// the logic of a program. Now, given these three variables and their
// respective data types, create three more string variables to contain a
// customer name, three more rank variables to contain each customer's rank,
// and three more boolean variables to contain whether or not a customer has
// paid or not. Be careful not to give these new variables the same names as
// the three variables above! Doing so will overwrite the values currently
// stored via "reassignment".

var name_two = "Darth Vader";
var name_three = "Han Solo";
var name_four = "Leia Organa Solo";

var rank_two = 1;
var rank_three = 4;
var rank_four = 3;

var paid_two = true;
var paid_three = false;
var paid_four = true;

// Before we continue, it's important to see other operators, and how the effect
// given data types. For example, with two Number data types,if we add:

var result = 2 + 2;
console.log("Result is:",result);

// But if we add two strings:

var string_result = "John" + "Cena"
console.log("String result is",string_result);

// Note that adding our two numbers resulted in the expected value of 4 being
// stored in our result variable. However, adding two strings actually ended up
// combining the two into a new string! Because of these differences, combining
// different data-types with a given operator will usually result either in
// unexpected behavior (results we don't want) or program error. This is why
// knowing the type of the data stored in a variable is so important. Below,
// we've listed all common operators for a given data-type, as well as logged
// the result of the operation to console:

// Add two numbers:
var num1 = 3;
var num2 = 7;
var add = num1 + num2;
console.log("Sum: ",add);

// Subtract two numbers:
var num1 = 3;
var num2 = 7;
var subtract = num2 - num1;
console.log("Difference: ",subtract);

// Multiply two numbers:
var num1 = 3;
var num2 = 7;
var multiply = num1 * num2;
console.log("Product: ",multiply);

// Divide two numbers:
var num1 = 3;
var num2 = 9;
var divide = num2/num1;
console.log("Quotient: ",divide);

// Add two strings:
var string1 = "Mark ";
var string2 = "Hamill";
var star = string1 + string2;
console.log("String concatenation: ",star);

// Makea boolean it's opposite truthiness (i.e. true -> false, false -> true)
var truth = false;
truth = !truth;
console.log("Truthiness of truth in: ",truth);

// Our basic data is setup! Let's take a look at how we can better store these
// variables in a JavaScript Object. Think of an Object as a container for
// multiple data that functions on a dictionary-like lookup system of "keys"
// and "values". In an object, a key is like the word in a dictionary under
// for which a definition (the analog of a value) is associated. Creating an
// object is as easy as creating any other variable:
//
//    var myObject = {};
//
// When we create an object this way, it is refered to as an "empty" object.
// This is because there are no key-value pairs (no data) currently stored
// inside the object. Let's fix that now! If we want to store Luke Skywalker's
// data inside an object, we would do the following. First, let's create a
// variable and set it equal to a blank object like below:
//
//    var Luke = {};
//

var Luke = {};
console.log(Luke);

//  Go ahead and log our new object to the console (just so we can make sure
// it's actually working. Next let's add in a "name" property. A "property" of
// a JavaScript Object means a key-value pairing. We can give our "Luke" object
// a "name" property by doing the following:
//
// Luke['name'] = name;
//
// Go ahead and try it below! If you want to see how our object has changed,
// log the results to console.

Luke['name'] = name;
console.log(Luke)

// Note that, whereas an empty set of curly brackets showed up in our console
// before, now those brackets contain something like below:
//
//    {name: "Luke Skywalker"}
//
// Logging your objects to console is a wise idea any time you manipulate them,
// as it allows you to see what and what type(s) of data are contained within
// your object, as well as the keys under which the values you need are stored.
// Now, lets add Luke's rank and paid status:
//
//    Luke['rank'] = rank;
//
//    Luke['paid'] = paid;
//

Luke['rank'] = rank;
Luke['paid'] = paid;

// Go ahead and log your Luke object to the console below to make sure you've
// stored your data correctly:

console.log(Luke);

// If all goes well, your Luke object should now look something like this in
// your browser's console:
//
//    {name: 'Luke Skywalker', rank: 2, paid: false}
//
// Now, just as we did above, store all your other objects inside Object data
// types! Be sure to ask questions if you get lost! We recommend using the key
// values used in the Luke Object above.

var Vader = {};
Vader['name'] = name_two;
Vader['rank'] = rank_two;
Vader['paid'] = paid_two;

var Han = {};
Han['name'] = name_three;
Han['rank'] = rank_three;
Han['paid'] = paid_three;

var Leia = {};
Leia['name'] = name_four;
Leia['rank'] = rank_four;
Leia['paid'] = paid_four;

// Be sure to log each new Object you've created to the console to ensure all
// data has been added correctly before moving on.

console.log(Vader);
console.log(Han);
console.log(Leia);

// Fantastic! We've just cut down the amount of data we have from twelve
// variables to four. But wait! How do we access a value of an object? It's
// as easy as calling value using the syntax below:
//
//    console.log(Luke['name'])
//
// Fantastic! Isn't there a cleaner way to add an access parameters of an object
// though? Those square brackets sure do take up a lot of space. Naturally,
// JavaScript has a beautiful, alternate method via "dot syntax". Dot syntax
// is an alternate method of accessing data in objects that allows for clean,
// concise setting and retrieval of a value of a given key. Try the example
// below in your own console:
//
// console.log(Luke.name)
//
// Sure enough, our console logs our Luke Object's name value the same as above.
// Given that we can access a parameter (recall, a parameter is a key-value
// pariring) with dot notation, this begs the question of whether or not we
// can also use dot notation to assign ore reassign a given key's value. The
// answer is yes! Let's try it below. Let's reassign our Luke Object's rank
// property to 1 (because obviously Luke is number one) as below:
//
//    Luke.rank = 1
//

  Luke.rank = 1

// Among the many advantages of JavaScript is that the language usually lets
// you manipulate data or tackle a given task a variety of different ways. Dot
// notation is just one of the many examples of this.
//
// We now have four customer objects, which makes accessing and manipulating
// our customer data easy. However, JavaScript has one more built-in data type
// that can help make our upcoming task easier. As with many modern programming
// languages, JavaScript has an Array data-type. Arrays are a foundational
// structure in much of programming and computer science as a whole, existing
// as an efficient and coder-friendly data-structure for storing large amounts
// of data. The best way to think of an Array is like a row of a vending
// machine. Every item in an Array has a "position" that can be accessed with
// the right input. Unlike a traditional vending machine, new data can be
// easily added to an Array. Enough talk, let's see a JavaScript array in
// action:
//
//    var myArray = [];
//
// That's it, an empty array! Easy, right? Go ahead and create an an empty
// Array below:

  var customers = [];

// Much like an Object, when you initially declare an Array this way, it
// contains no data in it. You *can* insert data to begin with, but lets
// leave that topic for a bit later. For now, lets add data to our Array the
// traditional JavaScript way, by using an Array data-type method, "push"!
// Like many data-types, Arrays in JavaScript (even empty ones) have built-in
// means of manipulating or changing it. Perhaps the best analogy for a
// data-type's method is a "power". Data-types in JavaScript come with these
// built in powers, and for an Array, push is one of the most useful. Go ahead
// and log your empty Array to console:

  console.log(customers);

// Now add one of your objects to your Array as below:
//
//    myArray.push(Luke)
//

  customers.push(Luke);

// Then log the result to console:

  console.log(customers);

// Much like with our Object, our array has gone from an empty set of brackets
// to containing our data! More importantly, notice the use of dot notation when
// using the "push" method, as well as two parens surrounding the data we pass
// in. Dot notation is an universal requirement for accessing JavaScript methods
// for data-types. Usually, you will pass one or more "arguments" into a given
// method inside the set of parens. This is because (underneath the hood)
// methods are actually functions! Go ahead and add your remaining Objects to
// the array you created using push, logging the result to console after each
// change.

  customers.push(Vader);
  console.log(customers);

  customers.push(Han);
  console.log(Han);

  customers.push(Leia);
  console.log(Leia);

// Fantastic! Our Array is loaded, and we now have a single variable storing
// all our data. Much as with Objects, we can access data in our Array as
// below:
//
//    myArray[0]
//
// Go ahead and try it below, logging the result to console.

  console.log(customers[0]);

// Note the use of the square bracket notation. Wait a minute, didn't we just
// see this when we were accessing a parameter of an Object? Don't panic,
// square bracket notation is used to access data in both objects and Arrays.
// However, something even more noticable is that to access the first piece
// of data stored in our array, we pass a value of "0"...
// ...
// What? Shouldn't the first thing in our Array be at position "1"? As with
// many programming languages, "elements" (data stored in a given position of
// an array) in a JavaScript array can be accessed by passing in the number in
// which they were added to/stored in the Array minus one. Or (for those
// prefering a more mathematical notation):
//
//    ***The i-th element of an Array of n elements is at position i-1****
//
// Write this down on your calendar, your notebook, you hand if you need to,
// because for *every* programming language you learn this holds true. Go ahead
// and log the second, third, and fourth elements of your array to the console:

console.log(customers[1]);
console.log(customers[2]);
console.log(customers[3]);

// What if we need to know how many elements are in our array though? Again,
// this can be done via the "length" method, available to both JavaScript Arrays
// and strings:
//
//  myArray.length
//
// Go ahead and log the length of your array to console now:

console.log(customers.length);

// If all goes well, the console should show that there are four elements in
// your Array! That's enough for now. Let's move on to some more basic
// JavaScript and put our data into action!

// Our task is to return a count of the number of customers that have paid,
// and an array of the customers that have not. Then perhaps the first part
// of our task that we ought accomplish is developing a means of determining
// whether or not a customer has paid. To start, let's see how we can combine
// dot and backet notation to access a given customer's paid status.
//
// Recall that in order to access a customer in our array, we use bracket
// notation, passing the position of the customer we want to retrieve minus one.
// Go ahead and log the third customer in your array to the console now:

  console.log(customers[2]);

// To check, lets log our entire customers array to console, and then work
// down the resulting output in console:

  console.log(customers);

// If your results match, you're already rocking! A key insight then is that
// when we access an element in our array this way, the array automatically
// returns the element being accessed. Recall then that we can use either
// bracket or dot notation to access a given parameter of an object. Then, to
// access the "paid" status of our third customer, let's use dot notation as
// below:
//
//    myArray[2].paid
//
// Go ahead and try it below, logging the result to your console:

console.log(customers[2].paid);

// The result? The boolean stating whether or not our third customer has paid
// their monthly membership dues is printed to the console! Now that we know
// this, go ahead and log the paid status of all of your other customers to
// console using the above combination of bracket and dot notation:

console.log(customers[0].paid);
console.log(customers[1].paid);
console.log(customers[3].paid);

// Now, let's formalize this concept through the introduction of "control
// statements". The common introduction for control statements in almost
// every programming course is that, as life requires decision making, so do
// programs. Fortunately, programming languages are much clearer regarding how
// these decision are made. With every programmign languages, the use of if,
// else, and else-if statements allows a programmer to dictate the flow of
// program logic. These statements rely upon the truthiness of conditions
// determined in "clauses" to execute code contained within a given control
// statement's block. Let's start with the JavaScript "if" statement. An "if"
// statement in JavaScript is written as below:
//
//    if(condition){
//      ...execute code here...
//    }
//
// If statements work as follows: *if* a conditional clause set inside the
// parens is true, the code inside the curly braces will execute. For an actual
// example, see the below:

  if(2 === 2){
    console.log("Two is two!");
  }

// This example seems toy-like, but belies the power these statments have in
// code. Note the use of a triple equals in comparing the two values. Unlike
// the single equals (which is the assignment operator, used to assign a
// value to a variable), the triple-equals is the "comparison operator". The
// triple equals takes one value or statement on the left and compares it to
// the value or statement on the right. If the two sides are equal both in
// *value and type*, then the If statement will evaluate to true and the code
// inside the curly brackets will execute. There area number of other comparison
// operators available to use in JavaScript, these include:

// If left-side is greater than right-side.
  if(3 > 2){
    console.log("Three is more than two!");
  }

// If left-side is less than right.side.
  if(2 < 3){
    console.log("Three is less than two!");
  }

// If left-side is greater than or equal to right side.
  if(2 >= 2){
    console.log("Two is greater than or equal to two!");
  }

// If right side is less than or equal to right side.
  if("Sami".length <= "John".length ){
    console.log("Name lengths are less than or equal!");
  }

// If not equal.
  if("Joe".length !== "John".length){
    console.log("Name lengths are not equal!");
  }

// If (also) not equal.
  if(!true){
    console.log("If not true is false!");
  }

// These are the basics of control-flow logic. Note that we can also combine
// multiple clauses in any conditional through the use of two additional
// operators:

// Check truthiness of both claue one AND (&&) clause two (ect.) are true:
  if(!false && (2 === 2)){
    console.log("All clauses are true!");
  }

// If left-side OR (||) right-side are true:
  if(!false || (2 > 3)){
    console.log("!false is true, so I still execute!");
  }

// Alright, that's enough introduction. Let's put conditionals to work for us
// by checking if the "paid" status of the third customer in our array is true
// as below:
//
//    if(myArray[2].paid){
//      console.log("Third customer has paid!");
//    }
//

  customers[2].paid = true;
  if(customers[2].paid){
    console.log("Customer two has paid.");
  }

// Note that if your third customer has not paid, nothing will log to console!
// If such is the case, go ahead and change the value of your third customer's
// paid parameter to true above the if statement, then run the code again!
//
// What if a customer hasn't paid through? Is there some control statement that
// we can use to execute code only if all other control statements above have
// failed to execute? Yes! This is where the else statement comes in. Code
// inside an else statement will only execute if all other conditional
// statements above have failed. An else statement must then be positioned
// below (after) all other conditional statements in a give block of code, and
// likewise their can be only one else statement per conditional block:

  if(2 === 3){
    console.log("I am true!");
  }
  else{
    console.log("Uh oh, the if statement wasn't true!");
  }

  if(3 !== 3){
    console.log("I'm true too!")
  }
  else{
    console.log("Nope, still not true!");
  }


// Awesome! But what if we want to check other conditions after an if statement
// (but before an else)? Introducing the else-if conditional! Like an else
// statments, else-if statements must be positioned *after* an if statement
// (but above an else statement). Unlike an else statement though, you may
// include as many else-if statements in a given conditional block as you want:

if(2===3){
  console.log("I am true!");
}
else if(2===(3-1)){
  console.log("No I am!");
}
else if(2===7){
  console.log("Neither of you are right, you know...");
}
else{
  console.log("Nothing is true...");
}

// Go ahead and write an if/else-if/else block with an if, two else-if, and an
// else block to check whether two customer ranks are either equal, the first
// greater than the second, the first less than the second. Inside each control
// statement, you should output a message stating the given condition is
// true, or (if all three non else statements fail) to log a message stating
// that there's been an error to console. Be sure to use the combined bracket
// and dot notation to access a customer's rank data:


if(customers[0].rank === customers[3].rank){
  console.log("Customers are of equal rank.")
}
else if(customers[0].rank > customers[3].rank){
  console.log("Customer one is of higher rank than customer four.");
}
else if (customers[0].rank < customers[3].rank) {
  console.log("Customer one is of lesser rank than customer four.")
}
else{
  console.log("Customer rank comparison: ERROR.")
}

// Now that you have a handle on if/else statements, we can implement an
// additional requirement. If a given customer has paid AND their rank is
// number one, the site grants them free membership. Else, if a customer has
// paid (but is not rank one), their status remains as paid for that month.
// Otherwise, if none of the above are applicable, that customer is added to a
// blacklist of customers who are delinquent in payment! Go ahead and try
// implementing this below, using if/else-if/else statements, combined backet
// dot notation, and ensuring that each statement logs an appropriate message
// to console if its conditional clause returns true. Do so for the first
// customer in your array. Note that for the else statement, you'll need to
// implement the push method on a new array you'll create to store delinquent
// customers:

var delinquent = [];

if(customers[0].paid === false && customers[0].rank === 1){
  console.log("Customer of Rank One has been exempted from payment.")
}
else if(customers[0].paid && customers[0].rank > 1){
  console.log("Customer has paid their dues.")
}
else{
  console.log("Customer is delinquent and has been blacklisted.");
  delinquent.push(cusomters[0]);
}

// Take a look for a moment and realize how far you've come. You're now
// implementing data instantiation, manipulation, and program control. That's
// something to be seriously proud of. Now, let's get ready to implement
// the final set of structures available to any JavaScript programmer, loops.
//
// In coding, there come tasks where repetition is innate. In completing these
// tasks, loops allow us to achieve our ends while minimizing duplicate code
// and maximizing efficiency. Think of it this way, would you rather the
// machine check the "paid" status of a million customers for you using a loop,
// or manually write the if/else blocks for each individual customer? The
// former is likely the more appealing choice. Most courses or books begin with
// simpler loop constructs such as while or do-while loops. We will covere these
// shortly, but due to the nature of fullstack development applications, it
// behoves us to understand the for loop first. Not only are for loops the most
// common loops you will use, they are arguably the easiest to understand (
// despite more moving parts than while or do-while loops). The structure of a
// for-loop looks something like this:
//
//    for(var i=start; i<end; i++){
//      ...execute code in here...
//    }
//
// There's a few parts here, so lets break it down. The first part of the loop
// is the declaration. Traditional for loops in JavaScript consist of the word
// "for" followed by parens and then curly brackets (which, much like control
// statements, contain the code to be executed by the loop). The first statement
// inside the parens initializes an "iterator" to some value. Note that, for
// a loop, such an iterator should always be a number. The inital value of an
// iterator variable is usually zero, but can be any other integer (whole
// number) value depending on wha the situation calls for.The second statement
// inside the parens is the control statement, a conditional clause that is
// checked at the beginning of each iteration (each run) of the loop. So long as
// the conditional clause returns true, the loop will continue to execute the
// code inside the curly brackets. The final statement is the increment
// statement, which controls how a loops iterator changes over each iteration.
//
// Note that conditional statements and initialization statements must work
// together, and can work in different ways than the example outlined above.
// Additional examples of viable for loops include:

// Increment statment inrements by one, i is initialized to zero.
  for(i=0; i<10; i++){
    console.log("Going up! ",i);
  }

// Increment statement decrements by one, i is initialized to ten.
  for(i=10;i > 0; i--){
    console.log("Going down! ",i);
  }

// Increment statement increments by two, i is initialized to zero.
  for(i=0; i<10; i+=2){
    console.log("Going up by two! ",i);
  }

// Increment statement increments by multiples of two, i is initialized to one.
// NOTE: This is particularly cool because it allows us to increment by binary
// powers of two!
  for(i=1; i<1-; i*=2){
    console.log("Going up by powers of two! ",i);
  }

// Decrement statement divides by two, i is initialized to sixteen.
// NOTE: Using division (as with the short-hand operator below) as a decrement
// in a loop is fine, but you need to make sure your iterator variable remains
// an integer.
  for(i=16; i>1; i/=2){
    console.log("Going down by powers of two! ",i)
  }

// Let's go ahead and get some practice below! Recall that you can access an
// item in your array by using bracket notation and passing in a number.
// Given that the increment variable of a loop is a number, then couldn't one
// pass the increment variable into the backets of an array to loop over that
// array and access each item in it? Yes! Give it a try below! Create a for
// loop, initializing your increment variable to zero (recall that items in
// arrays are at position i-1). The loop should iterate while the increment
// variable is strictly *less* that the length of the array (which can be
// accessed via the array's "length" method, see above for a refresh). Finally,
// your iterator variable should increment by one for each iteration of the
// loop. Go ahead and try out the short-hand syntax for increment by one (i++)
// as we did above for this part. Then, inside the curly brackets, log each
// item of your array to console!

  for(var i=0; i<customers.length; i++){
    console.log("Customer is: ",customers[i]);
  }

// Fantastic! We just through you the hardest loop in the book! Before we
// continue lets go over two more types of loops every programmer should know,
// the while and do-while loops. While loops appear as below:
//
//    var i=0;
//
//    while(i < n){
//      ...execute code in here...
//      i++;
//    }
//
// First, note the similarities. Like for loop, a while loop's parens contain a
// conditional that is checked prior to each iteration of the loop. The code
// contained within the brackets is executed so long as the conditional within
// the parens returns true. However, unlike for loops, we must initialize our
// iterator variable outside the loop, and create our increment statement
// within the curly brackets of the loop. While this might seem less clear than
// the syntax of a for loop, it allows for programmers to build loops using
// conditionals and logic of a wider variety than the strictly iterative logic
// of for loops. For example:
//
//    var name = "Sam";
//    var run = true;
//
//    while(run){
//      if(name === "Sam"){
//        console.log("Loop terminating");
//        run = false;
//      }
//    }
//
// While loops allow you to use booleans, string comparison, and many more types
// of conditionals to control the iterations of the loop. However, with this
// increased freedom comes increased responsibility. It is incredibly easy to
// accidentally code a while loop that runs forever due to a poorly defined
// conditional clause or forgetting to check/change the value of variables being
// checked by the conditional clause of the loop within that loop's brackets.
//
// Finally we reach the do-while loop. Do-while loops appear as below:
//
//    do{
//      ../execute code in here...
//    }while(condition)
//
// Do while loops work almost exactly like while loops save for one major
// difference. Where as traditional while loops check their conditional prior
// to each iteration, do-while loops check *after* each iteration. This leads
// to unique behavior where a do-while loop will always execute at least once.
//
// Enough talk, let's practice. As above, create a while loop to loop over your
// array of customers and print each to console. BE CAREFUL! Remember that you
// need to declare your iterator variable outside the while loop (set it to
// zero in this case) and that you must increment your iterator WITHIN the
// curly brackets of the while loop (by one, feel free to use the short-hand
// syntax). You conditional remains the same as with the for loop in that
// the loop should execute so long as the value of the iterator is less than
// the length of your array:

  var i = 0;

  while(i < customers.length){
    console.log("My customers are: ",customers[i]);
    i++;
  }

// Nicely done! We'll try to mostly use for loops from here on out, but be aware
// of while and do-while loops (they often crop up). For your next big challenge,
// we're going to construct a for loop to iterate over our array of customers.
// Inside the for loop, write the conditional if/else-if/else that we used
// earlier to check whether a customer had:
//
//    * Not paid but was of first rank
//    * Paid and was less than first rank
//    * Not paid and was less than first rank (resulting in being blacklisted)
//
// Don't forget you'll need to create a new, second array to push delequent
// customers to:

  var blacklisted = [];

  for(var i=0; i<customers.length; i++){
    if(!customers[i].paid && customers[i].rank === 1){
      console.log("Customer of rank one has used free pass.");
    }
    else if (customers[i].rank > 1 && customers[i].paid) {
      console.log("Customer of rank greater than one has paid.");
    }
    else{
      console.log("Customer has not paid and is less than rank one.");
      blacklisted.push(customers[i]);
    }
  }

// We're on the final stretch! We're now able to loop over our array of
// customers, checking whether that customer meets the various requirements of
// membership without having to clumsily repeat our conditional block for each
// entry. While our code is now much more flexible, there remains one last
// improvement we can make that will make our code even more reusable and
// friendly, wrapping it in a function!
//
// Functions are a core part of any programming language, allowing programmers
// to define and recall complex computational methods or processes while
// maximizing reusability and cleanliness of code. Functions in JavaScript come
// in several forms, but all utilize the keyword "function":

  function greeting(name){
    console.log("Hello ",name,"!");
  }

  greeting("Earthlings");

  var greeting = function(name){
    console.log("Hello ",name,"!");
  }

  greeting("Earthlings")

// Note that the two version of the function above are equivalent, differing
// only in the syntax used to declare them. Lets break down the first
// declaration of the greeting function above. First is the keyword "function",
// which is required by JavaScript (as it denotes what the following code is).
// Next is the function name. Although you can name your function anything, it's
// usually good practice to give a function a name that is informative and
// concise. Next, as with conditional clauses and loops, follows a set of
// parens and curly brackets. When the function is called (as on line 773),
// the function executes whatever code is inside the curly brackets.
//
// However, note that inside the parens is a variable! For functions, variables
// declared inside the parens are called "parameters". Parameters are variables
// that are designed to be assigned values whever the function is called, as
// can be seen on line 773 when we pass the greeting function's name parameter
// a value of "Earthlings". Unlike traditional variable, parameter variables
// are only available to code within the curly brackets of a function, and
// whatever value is passed to a parameter only remains available to the code
// inside that function while that function executes. This means, for example,
// if we create the addition function below:

  function add(a,b){
    var sum = a + b;
  }

// When we call the add function and pass two numbers:

  console.log("Result is: ",add(2,2));

// That the parameters "a" and "b" of our add function will only retain the
// values we passed of two and two for the duration it takes the computer to
// finish executing the code within our add function! Likewise, any variables
// we declare within the curly brackets of our function are only available for
// the like period of time. Then how do we get back the result of a function?
// The answer is a return statement:

  function add(a,b){
    var sum = a + b;
    return sum;
  }
  console.log("Result is: ",add(2,2));

// A return statement inside a function tells the computer that we are done
// using our function, and to pass back whatever is on the same line as the
// return to code outside the function. This means you should mainly use a return
// statement in a function once the code inside your function has completed its
// determined task and you need to get back the result of that computation!
// Then, to store the result of such computation, it makes sense to assign
// our add function to a variable when called, as below:

  var result = add(3,3);
  console.log("Result is: ",result);

// Rather than continuing to drone on about various uses and feature of
// functions it makes more sense to demonstrate their use by taking our for
// loop (with conditional block) above and wrapping it inside a function! Go
// ahead and create a function called "checkCustomers". It should contain
// both the blacklist array and the for loop (with conditional block) from
// above, have a single parameter called "customersIn", and return the blacklist
// array. Note that you will need to change the name of the array you use in
// both your for loop and conditional block to "customersIn" so that the
// function uses the array passed in as a parameter. Note that you can use
// either of the function declaration methods shown above:


  function checkCustomers(customersIn){
    var blacklisted = [];

    for(var i=0; i<customersIn.length; i++){
      if(!customersIn[i].paid && customersIn[i].rank === 1){
        console.log("Customer of rank one has used free pass.");
      }
      else if (customersIn[i].rank > 1 && customersIn[i].paid) {
        console.log("Customer of rank greater than one has paid.");
      }
      else{
        console.log("Customer has not paid and is less than rank one.");
        blacklisted.push(customersIn[i]);
      }
    }

    return blacklisted;
  }


// Fantastic! Go ahead and call your new function, passing your array of
// customers to the customersIn parameter. Wrap your function call in a console
// log statement and see the result of your hard work!

  console.log(checkCustomers(customers));

// There remains one more modification we must make to our function before we're
// done here. Inside your function, we need to add a variable to (outside the
// loop) to keep track of how many customers have paid, and then return this
// as well as the list of blacklisted customers. Go ahead and create the a
// variable inside your function just below the blacklist array called
// "paidCount". Then, inside your for loop, increment the paidCount variable
// ONLY if the customer is either of rank one and has not paid or has paid (and
// is of lower rank). You already have the conditional block checking for this,
// so your task is just to figure out where to stick two increment statements!
// Once you've done this, modify the return statement of your function as
// follows:
//
//    return {blacklist: blacklisted, paidCount: paidCount}
//
// Note that JavaScript functions do not explicitly allow for multiple return
// values from a function, as a result, we must wrap both pieces of data we
// want to return in an object with appropriately named keys.

function checkCustomers(customersIn){
  var blacklisted = [];
  var paidCount = 0;

  for(var i=0; i<customersIn.length; i++){
    if(!customersIn[i].paid && customersIn[i].rank === 1){
      console.log("Customer of rank one has used free pass.");
      paidCount++;
    }
    else if (customersIn[i].rank > 1 && customersIn[i].paid) {
      console.log("Customer of rank greater than one has paid.");
      paidCount++;
    }
    else{
      console.log("Customer has not paid and is less than rank one.");
      blacklisted.push(customersIn[i]);
    }
  }

  return {blacklist: blacklisted,paidCount: paidCount};
}

// Finally, let's call our function again, passing in our array of customers as
// before. Rather than logging the result to console, let's store our result
// in an outside variable called "customerResults".

var customerResults = checkCustomers(customers);

// Since our function now returns an object, if we want to log the blacklist and
// paidCount to console separately, we need only access them as we would values
// of any other object, by their respective key values! Go ahead and use dot
// notation to individually log the blacklist and paidCount stored inside our
// customerResults variable to console:

console.log("Blacklist: ",customerResults.blacklist);
console.log("Paid count: ",customerResults.paidCount);

// Congratulations! You've come far in understandind the basics of JavaScript!
// Be sure to keep this file handy, as we'll be adding much more to your
// JavaScript cheat-sheet in the weeks to come!
//
// Good luck and happy coding!
// -SC
