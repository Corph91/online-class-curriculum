// beverages

// beverage properties: name, price, category...
var b1  = { name: "coca-cola", price: "$3.00", category: "soda pop" };
var b2  = { name: "pepsi-cola", price: "$3.00", category: "soda pop" };
var b3  = { name: "bud-light", price: "$4.00", category: "alcohol" };
var b4  = { name: "green tea", price: "$1.00", category: "tea" };
var b5  = { name: "chamomile tea", price: "$10.00", category: "tea" };
var b6  = { name: "gatorade", price: "$2.50", category: "sports drink" };
var b7  = { name: "poweraide", price: "$2.00", category: "sports drink" };
var b8  = { name: "sprite", price: "$2.50", category: "soda pop" };
var b9  = { name: "7 up", price: "$2.00", category: "sports drink" };
var b10 = { name: "double haul", price: "$4.00", category: "alcohol" };


// console.log(b1.name);
// we need an array to hold these bevarages..
// lets loop through the array and print each item to the console..
var bevs = [b1, b2, b3, b4, b5, b6, b7, b8, b9, b10];



// JSON - javascript object notation
var DATA = {
	beverages: [b1, b2, b3, b4, b5, b6, b7, b8, b9, b10],
	meta: "1341232113123213123",
	headers: '1234124 dfasf '
};

var catBevs = [];

function loopThroughArr(arr, cat){

	for(var i = 0; i < arr.length; i++){

		if(arr[i].category === cat){
			catBevs.push( arr[i] );
		}
	};

	return catBevs;

}

console.log( loopThroughArr(DATA.beverages, "soda pop") );
