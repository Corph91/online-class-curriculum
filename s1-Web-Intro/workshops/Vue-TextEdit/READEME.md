The ability for a user to manipulate application data and have their changes
reflect immediately is an enticing functionality to add to any application. For
example, the ability to see one's order change automatically, or for the text
a user enters to to automatically reflect above or below a comment box. This
project will show you how to accomplish the latter, while illustrating concepts
such as two-way binding of data!

Two-way binding is a way tying input or data displayed on an HTML element to a
like data representation (i.e. a variable representing the data displayed) on
client-side JavaScript, such that if either the data is updated either by the
user (i.e. the user changes the input in the text field) or the JavaScript
(i.e. a new order is placed, and the server then updates data displayed to the
user to reflect a change in quantity) or the server, the data displayed and its
client-side JavaScript representation are effectively updated simultaneously
(and without the application or page needing to be refreshed to show the
change). Data tied to a VueJS app's "data" parameter are two-way bound, and
we're going to leverage this to build a real-time text edit box that
automatically updates the text displayed below as the user changes the text
in the input!

This project will also show how to use v-if and other Vue directives to enable
tighter control over user interface (again utilizing two-way binding) to display
our input textbox *only* when a user clicks on the displayed text field! To get
started, follow the below:

  1.) Clone the following repo:

        git clone https://Corph91@bitbucket.org/Corph91/textedit_vuejs.git

  2.) Run the command in your terminal/shell:

        npm install

  3.) Once done, create three files:

        a.) app.js

        b.) index.html

        c.) styles.css

Once you've finished the setup, follow along and (as always) be sure to ask
plenty of questions!


CREDIT:

The base code and idea for this exercise were taken from:

  http://tutorialzine.com/2016/03/5-practical-examples-for-learning-vue-js
