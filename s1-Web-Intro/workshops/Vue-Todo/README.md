As is time and tradition, we're going to look at creating a "ToDo" app using
VueJS! To begin you'll need to do the following:

  1.) Clone the following repo:

        git clone https://Corph91@bitbucket.org/Corph91/todo_vuejs.git

  2.) Run the command in your terminal/shell:

        npm install

  3.) Once done, create two files:

        a.) app.js

        b.) index.html

I'll then take you through the rest of creating our app. This is
designed to be a tutorial, so feel free to ask questions, make
comments, and stop me if you need to catch up. Once we have the basic
application running, I leave it to you to play with the front-end.
Can you add a sorting function to allow a user to display items in the
order in which they were added? What about an interactive button that
allows you to toggle the sort? Can you figure out how to add batch
deletion as well?

CREDIT:
This exercise and its code were taken from:

  https://scotch.io/tutorials/build-an-app-with-vue-js-a-lightweight-alternative-to-angularjs

I've made slight modifications to syntax or updated functionality to
better suit beginning web developers.
