Building off our previous exercise, we'll now implement a like custom filter
function in Vue to display only the data relevant to what the user enters. This
is an important function that you should be able to implement for any
data-driven website. To begin:

  1.) Clone the following repo:

        git clone https://Corph91@bitbucket.org/Corph91/search_vuejs.git

  2.) Run the command in your terminal/shell:

        npm install

  3.) Once done, create three files:

        a.) app.js

        b.) index.html

        c.) styles.css

Once you've finished the setup, follow along and (as always) be sure to ask
plenty of questions!


CREDIT:

The base code and idea for this exercise were taken from:

  http://tutorialzine.com/2016/03/5-practical-examples-for-learning-vue-js
