Interactive navbars are a must for modern websites, and VueJS makes building
one easy! The goal of this exercise is to introduce you to just how easy it is
to manipulate attributes of DOM elements like an HTML tag's class attribute
using a library like VueJS. To get started:

  1.) Clone the following repo:

        git clone https://Corph91@bitbucket.org/Corph91/navbar_vuejs.git

  2.) Run the command in your terminal/shell:

        npm install

  3.) Once done, create three files:

        a.) app.js

        b.) index.html

        c.) styles.css

After completing the above, let me know and I'll take you through the rest!
We're focusing on manipulating the class attribute of our menu, allowing us
to dynamically update which options shows the "active" class's CSS, as well
as what displays in the text below. Manipulating an HTML element's class
attribute is a valuable way to control user interaction and display of
information. Imagine that instead of displaying just an updated text value
in our app, clicking different tabs in our nav bar allows us to instead display
different div elements, each containing complex and unique information. In
doing so, we segment the presentation of our app as to appear to be several
different pages, however the app is actually contained within a single page!
This prevents annoying page refreshes while not overloading your user with
information (by attempting to pack too much into a single page, which usually
results in "scroll-itis", a terrible condition where a user has to scroll too
much to see what's on your page).

You challenge, upon finishing this tutorial, is to extend the functionality as
to display differing divs, each with a unique greeting, based upon what navbar
menu option the user has selected!


CREDIT:

The base code and idea for this exercise were taken from:

  http://tutorialzine.com/2016/03/5-practical-examples-for-learning-vue-js
