In this project, you'll get to see the true power of Vue. Utilizing the concepts
of DOM manipulation and two-way binding from previous exercises, we will build
a services menu that will automatically update the total cost of a given order
based upon what items the user selects, all without refreshing the application
or page! We'll also be building a custom *filter directive*. Until now, you've
been using Vue's built-in directives (think of directives as predefined
functionality that a programmer can implement across an application) to
manipulate data and your DOM. While that's fine and well, you will need to
be able to (and feel comfortable) in utilizing your growing knowledge of
writing functions to add functionality to libraries like Vue. You can create
a custom directive in Vue using the below syntax:

  Vue.filter('currency', function (value) {
      return '$' + value.toFixed(2);
  });

Note that this is much like calling a method on a JavaScript object! To get
started:

  1.) Clone the following repo:

        git clone https://Corph91@bitbucket.org/Corph91/textedit_vuejs.git

  2.) Run the command in your terminal/shell:

        npm install

  3.) Once done, create three files:

        a.) app.js

        b.) index.html

        c.) styles.css

After you've finished setup, follow along and ask questions! By the end, you'll
have a rockin' menu app you can show off or build upon by adding additional
services, cool graphics, or whatever else you want!


CREDIT:

The base code and idea for this exercise were taken from:

  http://tutorialzine.com/2016/03/5-practical-examples-for-learning-vue-js
