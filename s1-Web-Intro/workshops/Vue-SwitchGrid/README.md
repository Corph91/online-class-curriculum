Finally, we reach our last VueJS project! So far we've working on manipulating
DOM elements individually, as a list, or another easily achievable form. What
if we want to change the entire layout of a given page/view in a application
though? This is particularly common with mobile compatible web applications,
where screen space is limited and allowing a user to switch between more
visually concise and traditional forms of displaying large amounts of data
can make a huge difference in their experience.

Using two-way binding and DOM element attribute manipulation, we're going to
build a web application that allows a user to choose between a graphically
intuitive grid layout and a more traditional list layout of code articles.
To begin:

  1.) Clone the following repo:

        git clone https://Corph91@bitbucket.org/Corph91/switchgrid_vuejs.git

  2.) Run the command in your terminal/shell:

        npm install

  3.) Once done, create three files:

        a.) app.js

        b.) index.html

        c.) styles.css

Once you've finished the setup, follow along and (as always) be sure to ask
plenty of questions!


CREDIT:

The base code and idea for this exercise were taken from:

  http://tutorialzine.com/2016/03/5-practical-examples-for-learning-vue-js
