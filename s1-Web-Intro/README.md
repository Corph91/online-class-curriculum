Sprint One

Introduction to Web development

Core Topics:
- HTML
- CSS
- JavaScript (introductory)
- Vue.js

----

Reccomended Pace

Session One
- Liftoff / Plan / Introductions
- What Motivates You - you tube
- Tools: Zoom / Slack / CodePen / CodeRange / Trello
- Coding Assessment 45 min: http://codepen.io/fresh5447/pen/OWgXXW
- HTML foundations
  - use Code Pen to build a cheat sheet covering as many html concepts as possible.
-

Session Two
- Warmup
- CSS foundations
  - Use code pen to fork the HTML foundations cheat sheet. Apply and cover as many CSS rules and principles as possible.
- Bootstrap
  - Overview and Intro
  - Grid system
  - CSS
  - Components
  - Adding to your project
  - Code Along workshop

Session Three
- Warmup
- JSS foundations (http://codepen.io/fresh5447/pen/NppbKM?editors=0010)
  - Using Code pen, create working examples covering all of the javascript introductory concepts from variables to functions to arrays.
-

Session Four
- Ditching Code Pen
  - Introduction to Command Line
  - Into to text editors: Atom.io
  - Install Node.js
  - Downlad iTerm2

- How to use Node  static server
- Linking js/ index/ and css together
- Node Browser-Sync


Session Five
- JS Constructor Function Workshop: Making Fish
- Warmup: Code along random JavaScript
- Introduction to Vue.js

Session Six
- Warmup: Code along random JavaScript
- Make fish vue
- Retrospecive
