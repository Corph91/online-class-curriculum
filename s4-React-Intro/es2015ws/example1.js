// ------
// FUNCTIONS
// ------

// First Function Example

// old
function sayHi(name){
  return "Hello, " + name + "!"
};

// new
const sayHello = (name) => {
  return "Hello, " + name + "!"
};

// sayHello with template strings
const sayHello2 = (name) => {
  return `Hello, ${name}!`
};

// optional improvement:
// since it only retuns one thing, we can use the implicit
// return by ditching {}
const sayHello3 = (name) => `Hello, ${name}!`;

// optional improvement: Ditch the () since there is only
// one parameter being passed.
const sayHello4 = name => `Hello, ${name}!`;

// Second Function Example

// old
function getSum(a, b){
  return a + b
}


// new
const makeSum = (a, b) => {
  return a + b
}

// or
const makeSum2 = (a, b) => a + b;

// ------
// MAPPING
// ------

// old
var numbers = [1,2,3,5];

var mappedNumbers = numbers.map(function(item) {
  return item
});

// new
const nums = [1,2,3,5];

const mappedNums1 = numbers.map((item) => {
  return item
})

// if we get rid of the {}, the return becomes implicit
// meaning we no longer need to write it. So the above example
// could be written as:
const mappedNums2 = numbers.map((item) => item )
// we didn't need to put it all on one line but we
// it looks cleaner

// if a function only takes one param we can even ditch the ()
const mappedNums3 = numbers.map(item => item );

// ------
// Enhanced Object Literals
// ------

// old
var name = "Doug";
var age = "18";

var person = { name: name, age: age }


// new
// we use something called key value mirror. Often the names
// are the same, when this is true we can elminate typing it
// twice

const nombre = "Doug";
const anos = 18;

const persono = { nombre, anos }


// ------
// REACT
// ------

// Dumb Components

// old
//
// function ShowList(){
//   return (
//     <div>
//       <h1> Hello, world! </h1>
//     </div>
//   )
// }
//
// // new
// const ShowList = () =>
//   <div>
//     <h1> Hello, world! </h1>
//   </div>

// Smart Components

// old
var App = React.createClass({
	getInitialState: function(){
		return {
			list: null,
		}
	},
	componentDidMount: function() {
    this.setState({list: [1,2,4,5]})
	},

	render: function() {
		return (
			<div >
        <h1> Hello, world! </h1>
			</div>
		)
	}
})

// new
class App extends Component {
  state = {
    list: null
  }

	componentDidMount() => this.setState({list: [1,2,4,5]}

  makeList() => {
    this.state.list.map(item => item)
  }

	render() => {
    return (
      <div >
        <h1> Hello, world! </h1>
      </div>
    )
  }
}
