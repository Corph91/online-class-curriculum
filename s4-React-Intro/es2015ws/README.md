### Introducing ES6

In this workshop we are going to walk through a bunch of ES6 features, meanwhile also writing the ES5 code to compare it to.

All of the examples from babel are in codeSamples:
https://babeljs.io/learn-es2015/

Use these as a guide when teaching.

The main concepts to teach to the students are:

`let` & `const`
`arrow functions`
`enhanced objects`
`template strings`


Sets, Maps, Symbols, Proxies, and Promises should be held off for a later workshop. Only do so many new features in one session.

They will learn things like lexical this, spread operators, and classes, modules,  naturally through React.

----
----

Demos

Example one introduces let and const as well as arrow functions.

  - It compares ES5 to ES6 directly with examples of:
  - functions / arrows
  - template strings
  - mapping
  - enhanced object literals
