import React, { Component } from 'react';

class HeroesListContainer extends Component {
  render() {
    return (
      <div className="container">
        <h1> Post Hero Container </h1>
        <p> Some day I will be a component capable of creating a new hero </p>
      </div>
    );
  }
}

export default HeroesListContainer;
