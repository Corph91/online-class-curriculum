import React from 'react';

const HeroesList = (props) => {
  var heroItem = props.heroes.map((h, ind) => {
    return (
      <div key={ind}>
        <h1> { h.name } </h1>
        <ul>
          <li>{ h.universe }  </li>
          <li>{ h.rank }  </li>
        </ul>
      </div>
    )
  })
  return (
    <div className="container">
      <h1> Heroes List </h1>
      { heroItem }
    </div>
  )
}

export default HeroesList;
