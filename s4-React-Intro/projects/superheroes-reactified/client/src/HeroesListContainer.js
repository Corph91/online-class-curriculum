import React, { Component } from 'react';
import $ from 'jquery';

import HeroesList from './HeroesList';

class HeroesListContainer extends Component {
  state = {
    heroes: undefined
  }
  loadHeroes() {
    $.ajax({
      url: '/api/superheroes',
      method: 'GET',
    }).done( data => {
      this.setState({ heroes: data.data })
    })
  }
  componentDidMount = () => this.loadHeroes()
  render() {
    return (
      <div className="container">
        <h1> Hello, From Heroes Container! </h1>
        { this.state.heroes ? <HeroesList heroes={this.state.heroes} /> : "loading.." }
      </div>
    );
  }
}

export default HeroesListContainer;
