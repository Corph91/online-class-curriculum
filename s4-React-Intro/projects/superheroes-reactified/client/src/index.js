import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import App from './App';
import HeroesListContainer from './HeroesListContainer';
import HeroesPostContainer from './HeroesPostContainer';
import Home from './Home'
import './index.css';

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home}/>
      <Route path="/home" component={Home} />
      <Route path="/heroes" component={HeroesListContainer} />
      <Route path="/heroes/post" component={HeroesPostContainer} />
    </Route>
  </Router>,
  document.getElementById('root')
);
