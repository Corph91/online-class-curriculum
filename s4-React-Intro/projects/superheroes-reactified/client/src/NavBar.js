import React from 'react';
import { Link } from 'react-router';

const NavBar = (props) => {
  return (
    <div>
      <ul className="horizontal-flex">
        <Link to="/home"> Home </Link>
        <Link to="/heroes"> View Some Heroes </Link>
        <Link to="/heroes/post"> Make A Hero </Link>
      </ul>
    </div>
  )
}

export default NavBar
