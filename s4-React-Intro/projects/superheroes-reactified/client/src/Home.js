import React from 'react';

const Home = () =>
  <div className="container">
    <h1> Look at all of my heroes! </h1>
  </div>

export default Home;
