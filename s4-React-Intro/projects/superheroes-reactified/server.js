var express    = require('express');
    path       = require('path'),
    Villain    = require('./models/villain'),
    app        = express(),
    bodyParser = require('body-parser'),
    mongoose   = require('mongoose'),
    chalk      = require('chalk'),
    colors     = require('colors')
    heroRoutes = require('./routes/superheroes');

mongoose.connect("mongodb://localhost/superheroes");

app.set('port', 3001);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


const isProd = process.env.NODE_ENV === 'production';
const clientPath = isProd ? 'client/build' : 'client/public';

if (isProd) {
  app.use(express.static(clientPath));
}

app.get('/api/villains', function(req,res){
  Villain.find(function(err, data){
    if (err) {
      console.log(err);
    } else {
      res.json(data);
    }
  });
});



app.post('/api/villains', function(req,res){
    var newVillain = new Villain({
      name: req.body.name,
      evilPower: req.body.evilPower,
      evil: req.body.evil,
      nemesis: req.body.nemesis
    });
    newVillain.save(function(err,data){
      if (err) {
        console.log(err);
      } else {
        res.json(data);
      }
    });
});

app.put('/api/villains/:villain_id', function(req,res){
  Villain.findById(req.params.villain_id, function(err, vil){
    if(err) return err
    vil.loadPower(req.body.evilPower);
    vil.loadData(req.body);
    vil.save(function(e){
      if(e) return e
      res.json(vil);
    });
  });
});


app.use('/api/superheroes', heroRoutes);

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, clientPath, 'index.html'));
});

app.listen(app.get('port'), () => {
  console.log(chalk.blue("BEGIN COMPUTER STUFF 🤖 BEEEP 🤖 BOOOP 🤖 BOPPPPP 🤖"));
  console.log(`SERVER 🔥🔥🔥🔥 @  http://localhost:${app.get('port')}/`);
})


module.exports = app;
