Before students could only POST a new hero. We added an endpoint using Async that allows us to submit multiple heroes at once.

We did end up using Postman to send it Raw data (make sure you select application/json from the drop down), and send valid `JSON`.

Code Sample:

```
Router.route('/multiple')
  .post(function(req,res){
    var newHeroes = [];
    async.each(req.body.data, function(hero, cb) {
      var newHero = new Superhero();

      newHero.loadPower(hero.superPower);
      newHero.loadData(hero);

      newHero.save()
        .then(function(hero){
          console.log(hero, 'EACH HERO SUCCESS');
          newHeroes.push(hero);
          cb();
        }, function(err){
          if(err) cb(err);
        });
    },function(err) {
        if(err) throw err;
        res.json(newHeroes);
      });
  });
  ```
