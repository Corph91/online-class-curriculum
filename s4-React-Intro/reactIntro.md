Each code pen represents the end of the code pen. The goal should be to work your way up to the end in as small steps as possible, giving out challenges when appropriate. Stress props for this workshop only.


Hello World - Demonstrates how to render a basic component to a page. Can take a name through props - and return a greeting to that name.

http://codepen.io/fresh5447/pen/VbvgJd

Composite Components - Demonstrating how components can be made up of other components. Also demonstrates how components can be re usable.

http://codepen.io/fresh5447/pen/EmVqBx

Numbers Map and Filter - Begin by displaying the list of numbers, then works you way into filtering through the numbers.

http://codepen.io/fresh5447/pen/gWPYdB
