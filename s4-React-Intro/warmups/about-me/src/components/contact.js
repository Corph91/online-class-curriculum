import React from 'react';

const Contact = (props) => {
  return(
    <div className="contact-sec">
      <h3 className="sec-header">Contact: {props.info.name}</h3>
      <ul>
        <li>Phone: {props.info.phone}</li>
        <li>Email: {props.info.email}</li>
      </ul>
    </div>
  )
}

export default Contact
