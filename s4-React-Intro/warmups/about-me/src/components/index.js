export {default as Header} from './header.js';
export {default as NavBar} from './NavBar.js';
export {default as Content} from './content.js';
export {default as Contact} from './contact.js';
