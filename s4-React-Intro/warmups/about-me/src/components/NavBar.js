import React from 'react';
import './styles.css'

const NavBar = (props) => {
  return(
    <div>
      <ul className="nav-bar">
        <li>Home</li>
        <li>About</li>
        <li>Surprise</li>
        <li>Contact</li>
      </ul>
    </div>
  )
}

export default NavBar
