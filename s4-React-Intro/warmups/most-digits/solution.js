let numbers = [1, 11, 0.9, 2.21, 14, 555, 9000, 221, 3,
               764.23, 221, 2, 80000, 90, 22, 21, 8,
               5000000,22, 8921, 802.8921, 221.33, 82, 239,
               2.221,32.39082,50.000000000001, 21.2, 23.211]


// Function uses fast-casting to cast num to string,
// replace decimal and get length of number.
const numDigits = (numIn) => (numIn + '').replace('.', '').length


// Function takes array as argument and loops
// through using for-each method. Gets length
// of each num and uses O(n) global max alg.
// to find max length.
const findDigits = (numsIn) => {

  let maxNum = numsIn[0]
  let maxLen = numDigits(maxNum)

  numsIn.forEach((num, index) => {
    let length = numDigits(num)

    if(length > maxLen){
      maxLen = length
      maxNum = num
    }
  })
  return maxNum
}

console.log("The number with the most digits is:",findDigits(numbers))
