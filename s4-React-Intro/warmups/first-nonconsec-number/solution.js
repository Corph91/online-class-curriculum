let numbers = [1,2,3,4,5,7,8,9,10,11,14]

// This is akin to a distance problem. We want to
// make sure that the "distance" between two numbers
// does not exceed one, else they are non-consecutive.
//
// To accomplish this, we can utilize the Math library's
// "abs" (for absolute) function to get the absolute
// difference (the "distance") between two numbers.
// We then check to see if that distance exceeeds
// one. If so, we log the numbers and their positions
// in the array to console and use aa break statement
// to end the for-loop's execution.


for(let i=1; i<numbers.length; i+=1){
  if(Math.abs(numbers[i] - numbers[i-1]) !== 1){
    console.log(`Non-consecutive numbers: ${numbers[i-1]} and ${numbers[i]} found.`)
    console.log(`Positions: ${i-1} and ${i}`)
    break;
  }
}
