# Trash Can

Your goal is to create a "trash can" object! It should store any items
that are thrown away (using the "throwAway" function) under the "trash"
property. Of course, just in case we accidentally throw something away, we
should also be able to retrieve any item accidentally thrown away via a
"takeBack" function.

__Examples:__

```
trashCan.throwAway("coke bottle"); // -> trashCan object now has "coke bottle"

trashCan.throwAway("old shoes"); // -> trashCan object now has "coke bottle","old shoes"

trashCan.throwAway("Nintendo Switch") // -> trashCan object now has "coke bottle", "old shoes", "Nintendo Switch"

trashCan.takeBack("Nintendo Switch") // -> Returns "Nintendo Switch" string.

```
