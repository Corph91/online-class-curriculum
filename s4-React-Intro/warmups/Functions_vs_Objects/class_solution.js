var Vehicle = class{
  constructor(type,wheels,motor){
    this.type = type;
    this.wheels = wheels;
    this.motor = motor;
    this.gear = 0;
    this.started = false;
  }

  startCar() {
    this.started = !this.started;
    (this.started) ? console.log("Car running!") : console.log("Car turned off!")
  }

  shiftUp() {
    if(this.started && this.gear < 6){
      this.gear += 1
      console.log("Current gear:",this.gear)
    }
    else if (this.started && this.gear === 6) {
      console.log("Hit max gear! Can't upshift!");
    }
    else{
      console.log("Can't upshift! Car isn't started!");
    }
  }

  shiftDown() {
    if(this.started && this.gear > 0){
      this.gear -= 1
      console.log("Current gear:",this.gear)
    }
    else if (this.started && this.gear === 0) {
      console.log("In neutral! Can't downshift!");
    }
    else{
      console.log("Can't downshift! Car isn't started!");
    }
  }

  aboutMe() {
    console.log("I am a",this.type,"with",this.wheels,"wheels and a",this.motor,"motor.")
  }
}

let Audi = new Vehicle("Audi",4,"v6")
Audi.aboutMe()
Audi.startCar()

for(let i=0; i<10; i+=1){
  Audi.shiftUp()
}

for(let i=0; i<10; i+=1){
  Audi.shiftDown()
}
