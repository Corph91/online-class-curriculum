let Vehicle = {
  motor: undefined,
  wheels: undefined,
  type: undefined,
  gear: 0,
  started: false,
  startCar: function(){
    this.started = !this.started;
    (this.started) ? console.log("Car started!") : console.log("Car turned off!")
  },
  shiftGearUp: function(){
    if(this.started && this.gear < 6){
      this.gear += 1
      console.log("Current gear:",this.gear)
    }
    else if (this.started && this.gear === 6) {
      console.log("I've hit max gear!")
    }
    else{
      console.log("I can't shift gears, car isn't started!")
    }

  },
  shiftGearDown: function(){
    if(this.started && this.gear > 0){
      this.gear -= 1
      console.log("Current gear:",this.gear)
    }
    else if (this.start && this.gear === 0) {
      console.log("I'm in neutral! Can't downshift!");
    }
    else{
      console.log("I can't shift gears, car isn't started!");
    }
  },
  aboutMe: function(){
    console.log("My car is a:",this.type,"with a:",this.motor,"engine and",this.wheels,"wheels")
  }
}

Vehicle.motor = "V6"
Vehicle.wheels = 4
Vehicle.type = "Audi"



Vehicle.aboutMe()
Vehicle.startCar()
console.log("Vehicle is now running!")

for(let i=0; i<10; i+=1){
  Vehicle.shiftGearUp()
}

for(let i=0; i<10; i+=1){
  Vehicle.shiftGearDown()
}
