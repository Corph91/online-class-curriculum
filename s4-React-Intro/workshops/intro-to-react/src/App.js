import React, { Component } from 'react';
import WebApp from './WebApp'
import './styles.css';

const Hello = () => {
  return(
    <div>
      <h1>Hello!</h1>
      <h3>Welcome to Intro to React</h3>
    </div>
  )
}

const navData = ['Home','About','Contact','Blog']

class App extends Component {

  render() {
    return (
      <div>
        <WebApp navData={navData}/>
        <Hello />
      </div>
    );
  }
}

export default App;
