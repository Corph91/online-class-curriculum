import React from 'react';
import './styles.css'

const NavBar = (props) =>
  <nav>
    <ul>
      {
        props.navData.map((item,index) => <li key={index}>{item}</li>)
      }
    </ul>
  </nav>



const Jumbotron = (props) => <div className="jumbotron app-jumbo"><h1>#{props.title}</h1></div>

const WebApp = (props) => {
  return(
    <div>
      <NavBar navData={props.navData}/>
      <Jumbotron title={'WOW'}/>
      <h1>{'Welcome to my app!'}</h1>
      <h3>{'This is actually just JavaScript'}</h3>
    </div>
  )
}

export default WebApp
