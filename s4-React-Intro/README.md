_Sprint Four_

#### Introduction to React

## Core Topics:
- Intro To React
  - props & state
  - JSX
  - Create React App
  - class & stateless components

----

_Recommended Pace_

----

Session One: Keep it simple and do multiple examples of stateless components using Code Pen.

- Warmup: Introduce Async and add to Superheroes. Adds ability to batch submit heroes. _See asyncHeroes.md_

- Intro to React: Code Pen samples of using stateless functions to compose react components.
  - _See reactIntro.md_

  ----

Session Two
- Warmup: `TexMex` code pen: Similar as the number challenge.. but with more realistic data. End goal is to filter out food items that are too spicy.

http://codepen.io/fresh5447/pen/ybeBde

- React: State vs Props.
  - Code pens that show off class components that utilize and mutate state.
  - A counter, toggler, and mini todo list.
  - _see stateIntro.md_

----

Session Three
- Warmup: Space React Presentation by Spencer
- Implement Todo List Code Pen React
- Create-React-App: Add todo list
  - http://codepen.io/fresh5447/pen/GmZQmE

----

Session Four
- Warmup
- ES5 to ES6 Workshop -> see `es2015ws`
- Begin Superheroes React Conversion
  - Clone heroes, spin up react, hook two apps together using procfile and proxy.


Note about Superheroes project: Rather then branching, I was very specific about my commits:

First commit is the fully functional database
Second commit shows how to proxy the node/react server together
Third commit gets rid of generated react stuff
Fourth Commit Adds the component that gets our List of Superheroes
Fifth commit adds react Router
... away we go with react from there!

----
Session Five
- Warmup: Convert this ES5 to ES6 type challenge
- React-Router Workshop
- Add Router to heroes
- Continue client hero crud

----

Session Six
- Warmup: Convert this ES5 to ES6 type challenge using React
- Finish Superhero React
- Retrospective

----
