process.env.NODE_ENV = 'test'

const mongoose = require('mongoose')
const Superhero = require('../models/Superhero')
const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../server')
const should = chai.should()

chai.use(chaiHttp)

describe('Superheroes', () => {
    beforeEach((done) => { //Before each test we empty the database
        Superhero.remove({}, (err) => {
           done();
        });
    });
/*
  * Test the /GET route
  */
  describe('/GET superheroes', () => {
      it('it should GET all the superheroes', (done) => {
        chai.request(server)
            .get('/api/superheroes')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.data.should.be.a('array');
                res.body.data.length.should.be.eql(0);
              done();
            });
      });
  });

  describe('/POST superhero', () => {
      it('it should not POST a superhero without all fields', (done) => {
        let hero = {
            name: "Iron Man"
        }
        chai.request(server)
            .post('/api/superheroes')
            .send(hero)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.message.should.have.property('errors');
                res.body.should.have.property('data');
                res.body.should.have.property('data').eql(null);
              done();
            });
      });

      it('it should POST a superhero with dates and all fields', (done) => {
        let hero = {
          name: 'Iron Man',
          superpower: 'Lasers',
          image: 'n/a'
        }
        chai.request(server)
            .post('/api/superheroes')
            .send(hero)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('message')
              res.body.message.length.should.be.above(0)
              res.body.should.have.property('data')
              res.body.data.should.be.a('object')
              res.body.data.should.have.property('name')
              res.body.data.should.have.property('superpower')
              res.body.data.should.have.property('image')
              res.body.data.should.have.property('created')
              res.body.data.should.have.property('modified')
              done()
            })
      })

  });

  describe('/GET/:id superhero', () => {
    it('it should GET a superhero by the given id', (done) => {
      let created = new Date();
      let hero = new Superhero({ name: "Iron Man", superpower: "Lasers", image: 'n/a', modified: created, created: created});
      hero.save((err, hero) => {
          chai.request(server)
          .get('/api/superheroes/' + hero._id)
          .send(hero)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message')
            res.body.message.length.should.be.above(0)
            res.body.should.have.property('data')
            res.body.data.should.be.a('object')
            res.body.data.should.have.property('name')
            res.body.data.should.have.property('superpower')
            res.body.data.should.have.property('image')
            res.body.data.should.have.property('created')
            res.body.data.should.have.property('modified')
            done();
          });
      });
    });
  });

  /*
  * Test the /PUT/:id route
  */
  describe('/PUT/:id book', () => {
    it('it should UPDATE a superhero given the id', (done) => {
      let created = new Date();
      let hero = new Superhero({ name: "Iron Man", superpower: "Lasers", image: 'n/a', modified: created, created: created});
      hero.save((err, hero) => {
        chai.request(server)
            .put('/api/superheroes/' + hero._id)
            .send({superpower: 'Flight'})
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('message')
              res.body.message.length.should.be.above(0)
              res.body.should.have.property('data')
              res.body.data.should.be.a('object')
              res.body.data.should.have.property('name')
              res.body.data.should.have.property('superpower')
              res.body.data.should.have.property('image')
              res.body.data.should.have.property('created')
              res.body.data.should.have.property('modified')
              res.body.data.superpower.should.eql('Flight')
              done();
            });
          });
      });
   });

   /*
  * Test the /DELETE/:id route
  */
  describe('/DELETE/:id superhero', () => {
      it('it should DELETE a superhero given the id', (done) => {
        let created = new Date();
        let hero = new Superhero({ name: "Iron Man", superpower: "Lasers", image: 'n/a', modified: created, created: created});
        hero.save((err, hero) => {
                chai.request(server)
                .delete('/api/superheroes/' + hero._id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('data');
                    res.body.data.should.be.a('object');
                    res.body.data.should.eql({});
                  done();
                });
          });
      });
  });

});
