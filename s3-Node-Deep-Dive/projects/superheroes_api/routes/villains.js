const express = require('express')
const Router = express.Router()
const Villain = require('../models/Villain')

Router.route('/')
  .get((req,res) => {
    Villain.find((err, Villaines) => {
      if(err){res.json({message: err, data: null})}
      else {res.json({message: `Successfully retrieved all villaines!`, data: Villaines})}

    })
  })
  .post((req,res) => {
    // Create a new, blank villain
    let newVillain = new Villain()
    newVillain.loadData(req.body)
    newVillain.setMetaDates()
    newVillain.save((err, newVillain) => {
      if(err){ res.json({message: err, data: null})}
      else{res.json({message: `Successfully created new villain: ${newVillain.name}`, data: newVillain})}
    })
  })

Router.route('/search/:query')
  .get((req,res) => {
    Villain.find({'name': {'$regex': req.params.query, '$options': 'i'}}, (err, Villain) => {
      if (err){res.json({message: err, data: null})}
      else{res.json({message: `Successfully retrieved villain: ${Villain.name}`, data: Villain})}
    })
  })

Router.route('/:villain_id')
  .get((req,res) => {
    Villain.findById(req.params.villain_id, (err, Villain) => {
      if(err){res.json({message: err, data: null})}
      else{res.json({message: `Successfully retrieved villain: ${Villain.name}`, data: Villain})}
    })
  })
  .put((req,res) => {
    Villain.findById(req.params.villain_id, (err, Villain) => {
      Villain.loadData(req.body)
      Villain.setMetaDates()
      Villain.save((err, Villain) => {
        if(err){res.json({message: err, data: null})}
        else{res.json({message: `Successfully updated villain: ${Villain.name}`, data: Villain})}
      })
    })
  })
  .delete((req,res) => {
    Villain.remove({_id: req.params.villain_id}, (err) => {
      if(err){res.json({message: err, data: null})}
      else{res.json({message: `Villain successfully deleted!`, data: {}})}
    })
  })


module.exports = Router;
