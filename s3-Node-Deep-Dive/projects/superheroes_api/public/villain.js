const title = "Villains Advanced"
const villainFormTitle = "Create New Villain"

const app = new Vue({
  el: '#app',
  data: {
    title: title,
    villains: undefined,
    villainFormTitle: villainFormTitle,
    name: '',
    superpower: '',
    image: '',
    selectedVillain: undefined,
    villainIndex: undefined,
    showEdit: false,
    searchVillain: '',
    invert: true

  },
  created(){
    this.loadData()
  },
  methods: {
    loadData: function(){
      let self = this

      $.ajax({
        url: '/api/villains',
        method: 'GET'
      }).done((response) => self.villains = response.data)
    },
    createVillain: function(){
      let self = this
      let newVillain = {
                      name: this.name,
                      superpower: this.superpower,
                      image: this.image
                    }

      $.ajax({
        url: '/api/villains',
        method: 'POST',
        data: newVillain
      }).done((response) => window.location.href = "/villains.html")
    },
    deleteVillain: function(villain_id){
      let self = this

      $.ajax({
        url: `/api/villains/${villain_id}`,
        method: 'DELETE'
      }).done((response) => self.loadData())
    },
    toggleEdit: function(villain_id, index){
      this.showEdit = !this.showEdit
      this.selectedVillain = (this.showEdit) ? villain_id : undefined
      this.villainIndex = (this.showEdit) ? index : undefined
    },
    editVillain: function(){
      let self = this
      let modifiedVillain = {
        name: (self.name.length !== 0) ? self.name : null,
        superpower: (self.superpower.length !== 0) ? self.superpower : null,
        image: (self.image.length !== 0) ? self.image : null
      }

      $.ajax({
        url: `/api/villains/${self.selectedVillain}`,
        method: 'PUT',
        data: modifiedVillain
      }).done((response) => {
        self.villains[self.villainIndex] = response.data
        self.showEdit = false
      })
    },
    searchVillains: function(){
      let self = this
      if(self.searchVillain.length > 0){
        $.ajax({
          url: `/api/villains/search/${self.searchVillain}`,
          method: 'GET'
        }).done((response) => self.villains = response.data)
      }
      else{
        this.loadData()
      }

    },
    sortBy: function(field){
      let self = this

      self.invert = !self.invert

      self.villains = self.villains.sort((a,b) => {
        if(self.invert){return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0)}
        else{return (a.name > b.name) ? -1 : ((b.name > a.name) ? 1 : 0)}
      })


    }
  }
})
console.log('Hello!');
