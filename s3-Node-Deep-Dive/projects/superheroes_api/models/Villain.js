const Promise = require('bluebird')
const mongoose = Promise.promisifyAll(require('mongoose'))

const VillainSchema = new mongoose.Schema({
  name: {type: String, required: true, default: null},
  superpower: {type: String, required: true, default: null},
  image: {type: String, require: true, default: null},
  created: {type: Date, required: true},
  modified: {type: Date, required: true}
})

VillainSchema.methods.loadData = function(dataIn){
  this.name = (dataIn.name) ? dataIn.name : this.name;
  this.superpower = (dataIn.superpower) ? dataIn.superpower : this.superpower;
  this.image = (dataIn.image) ? dataIn.image : this.image;
}

VillainSchema.methods.setMetaDates = function(){
  let postDate = new Date()

  this.created = (!this.created) ? postDate : this.created;
  this.modified = postDate;
}

module.exports = mongoose.model('Villain',VillainSchema);
