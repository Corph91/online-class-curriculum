## BSCA: Online Full Stack javascript
#### Course Resources

This is a working repository to serve as a collection of all relevant resources for teaching the online full stack javascript course at the Big Sky Code Academy.

Outline & Overview

The 5 month course is outlined into 2 week sprints. Each sprint is contained in a subdirectory with relevant resources.

----

### _Sprint 1_
#### Introduction to Web development

Core Topics:
- HTML
- CSS
- JavaScript (introductory)
- Vue

----

### _Sprint 2_
#### Introduction to Backend Development with NodeJS

Core Topics:
- Introduction to Node
- Intro To TDD
- Rest & APIs
- Vue.js
- Superheroes Project


----

### _Sprint 3_
#### Backend Development In-depth with NodeJS


Core Topics:
- MongoDB
- Test Driven Development (TDD) with Chai / Mocha
- Test REST Api with Chai HTTP
- Vue.js
- Superheroes Project cont'd

----

### _Sprint 4_
#### Introduction to React

Core Topics:
- React Intro
- Using React in Code Pen
- smart and presentational components
- composite components
- Create React App by FB

----

### _Sprint 5_
#### Designing User Experience

Core Topics:
- Sketch
- ES6
- Blog Project kickoff


----

### _Sprint 6_
#### Advanced React, Blog Projects

Core Topics:
- ES6
- Blog Project kickoff

----

### _Sprint 7_
#### Blog Projects Cont'd

Core Topics:
- Heroku
- Blog Project to Completion


----

### _Sprint 8_
#### DB Deep dive: PostgreSQL

Core Topics:
- Relational DBS vs Non-relational
- Sequelize & PostgreSQL


----

### _Sprint 9_
#### Group Projects / Job Prep

Core Topics:
- Launch group Projects
- Job prep

----

### _Sprint 10_
#### Group Projects Cont'd / Finalize Job Prep

Core Topics:
- Bring group projects to completion
- Continue Job Prep
