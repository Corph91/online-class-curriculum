import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Toggler from './togglerComponent.js';

class App extends Component {
  state = {
    showing: false,
  }
  showInfo = () => this.setState({showing: !this.state.showing});
  render() {
    return (
      <div>
        <Toggler showInfo={this.showInfo} showing={this.state.showing}/>
      </div>
    );
  }
}

export default App;
