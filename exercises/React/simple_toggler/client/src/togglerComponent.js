import React from 'react';

const Toggler = (props) => {
  return(
    <div>
				<div className="jumbotron">
					<h4> Look at my awesome website!!</h4>
					<button onClick={() => props.showInfo()}> Toggle Details </button>
				</div>
				{ props.showing ?<p> This is a whole bunch of details we are goin to show and hide! </p> : undefined }
			</div>
  )
}

export default Toggler
