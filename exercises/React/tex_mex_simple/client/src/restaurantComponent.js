import React from 'react';


const Restaurant = (props) => {
  return(
    <div>
      <div className="jumbotron">
        <h3> { props.title } </h3>
        <p> { props.phone }</p>
        <ul>
          {
            props.menu.filter((food) => food.spiceLevel > props.spiceThresh).map((food, index) => {
              return (
                <li key={index}>{food.item}</li>
              )
            })
          }
        </ul>
      </div>
    </div>
  )
}

export default Restaurant
