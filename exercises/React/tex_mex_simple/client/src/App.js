import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Restaurant from './restaurantComponent';

class App extends Component {
  state = {
    menu: undefined,
    phone: "(404) 788-4545",
    title: "Juans Burrito Stop",
    spiceThresh: 5

  }
  componentDidMount = () =>   this.setState({menu: [
    { item: "Pork Tacos", spiceLevel: 5 },
    { item: "Enchilada", spiceLevel: 7 },
    { item: "Tamale", spiceLevel: 3 },
    { item: "Chalupa & Enchilada", spiceLevel: 8 },
    { item: "Chile Relleno", spiceLevel: 2 },
    { item: "Cheese Crisp", spiceLevel: 2 },
    { item: "Inferno Cheese Crisp", spiceLevel: 9 }
    ]})
  render() {
    return (
      <div>
        {this.state.menu ? <Restaurant menu={this.state.menu} phone={this.state.phone} title={this.state.title} spiceThresh={this.state.spiceThresh}/> : null}
      </div>
    );
  }
}

export default App;
