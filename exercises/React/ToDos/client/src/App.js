import React, { Component } from 'react';
import {Spinner, TodosList} from './Components';

class App extends Component {
  state = {
    todos: undefined,
    newTodo: undefined
  }
  componentDidMount = () => this.loadPage()
  loadPage(){

    let data = [
         { text: 'clean room'},
         { text: 'do laundry'},
         { text: 'walk dog'},
         { text: 'learn to spell'}
       ]

    let self = this;
		setTimeout(function(){
			self.setState({ todos: data })
		}, 500);
  }
  updateTodoText = (event) => this.setState({newTodo: event.target.value})
  submitTodo = this.submitTodo.bind(this)
  submitTodo(event){
    event.preventDefault()
    let todo = {text: this.state.newTodo}
    this.setState({todos: this.state.todos.concat([todo])})
  }
  deleteTodo = this.deleteTodo.bind(this)
  deleteTodo(todo){
    let newTodos = this.state.todos
    newTodos.splice(newTodos.indexOf(todo),1)
    this.setState({todos: newTodos})

  }
  render() {
    return (
      <div>
        {this.state.todos ? <TodosList todos={this.state.todos} updateTodoText={this.updateTodoText}
                              submitTodo={this.submitTodo} deleteTodo={this.deleteTodo}/> : <Spinner /> }
      </div>
    );
  }
}

export default App;
