import React from 'react';
import './spinnerStyles.css'

const Spinner = (props) => {
  return(
    <div>
      <i className="fa fa-spinner fa-spin fa-5x fa-fw rainbowz me-spinner"></i>
    </div>
  )
}

export default Spinner;
