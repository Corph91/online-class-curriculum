import React from 'react';
import './todosStyles.css'


const TodosList = (props) => {
  return(
    <div>
      <div className="jumbotron">
        <h3 className="rainbowz"> Todo List! </h3>
      </div>
      <ul>
        {
          props.todos.map((item) => {
            return(
            <li onClick={() => props.deleteTodo(item)}>
              <i className="fa fa-check item-icon"></i>
              {item.text}
            </li>
            )
          })
        }
      </ul>
      <div>
        <form onSubmit={(event) => props.submitTodo(event)}>
          <div className="col-md-12">
            <input placeholder="What do you need to do?" onChange={(event) => props.updateTodoText(event)} />
          </div>
          <div className="col-md-12">
            <button type="submit" className="btn btn-success submit-button">Submit</button>
          </div>

        </form>
      </div>
    </div>
  )
}

export default TodosList
