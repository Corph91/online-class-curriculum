/*
 * EXERCISE 12: Shuffling
 *
 * This exercise puts much of what we've learned thusfar to solve one of the
 * most intersting problems in computer science, shuffling (or randomizing
 * arrays of data in general). The algorithm we'll create here is called a
 * "Knuth Shuffle" (named after famous computer scientist and professor Donald
 * Knuth), and represents one of the most efficient shuffling algorithms
 * available. Let's begin by thinking about the structure and components of
 * our problem: we're looking to shuffle a deck (in this case, we'll assume
 * a standard deck of fifty-two "cards"). Think about how you could represent
 * such a deck in a computer program. One practical method would be some string
 * containing both a card's rank and suit, for example:
 *
 *    "9H"
 *
 * would be the nine of hearts. Likewise then:
 *
 *    "QD"
 *
 * could be the queen of diamonds. String representation of our deck of cards
 * would then be efficient and compact. For the sake of this exercise, we're
 * going to opt for an even simpler representation, an array for numbers from
 * 1-52 (inclusive).
 *
 * In past exercises, I've largely provided you the initial data. It's time to
 * notch up the challenge a bit and have you figure out how to create your own.
 * Don't worry, I'll walk you through as always, but you'll be responsible for
 * the actual code implementation. Let's begin by thinking about how we could
 * create our deck (our array of numbers from 1-52). A for loop seems like a
 * good choice, as it can be set to iterate fifty-two times quite easily.
 * We could iterate through the loop, starting at one and ending with the
 * iterator is greater than fifty-two, pushing the iterator to our initial
 * array.
 *
 * It would also be nice to be able to repeatedly call this behavior (what if
 * we want to make more than one deck). Then encapsulating our loop in a
 * function seems reasonable as well. Create a function then called
 * "generateDeck" this function should take no arguments. For now, add a
 * console log statement inside your function and then call it below:
 *
 *    function generateDeck(){
 *      console.log("Generate Deck working.");
 *    }
 *
 *    generateDeck();
 *
 * Then, in your terminal or command prompt, run the command:
 *
 *    node <filename.js>
 *
 * Your log statement should print to terminal/command prompt shell. Once
 * we know our function is at least minimally working, let's flesh out its
 * functionality.
 *
 * There are several ways we could go about creating our deck. We could add an
 * argument to our function and pass any array we instantiate outside our
 * function to a function call. However, we're going to opt for a more direct
 * method by generating deck inside the function, then returning this deck to
 * an outside variable. The reasoning behind this method is that this ensures
 * both that:
 *
 *    1.) The purpose and effect of the function is kept "pure", i.e. it won't
 *        have to deal with potenitally corrupt outside data. If we create
 *         our deck internally then pass it back to a new variable via return
 *         statement, so long as the function works correctly, we *know* that
 *         the variable to which the function call is assigned is a valid deck.
 *
 *     2.) Cleanliness of code.
 *
 * Inside your function, create an empty array called "tempDeck":
 *
 *    function generateDeck(){
 *      var tempDeck = [];
 *    }
 *
 * Below, declare your for loop. Recall that we have fifty-two cards, but that
 * their numbers must start at one and include fifty-two. Then go ahead and
 * implement the for loop as below.
 *
 *    function generateDeck(){
 *      var tempDeck = [];
 *
 *      for(var i=1; i<=52; i+=1){
 *
 *      }
 *
 *    }
 *
 * Inside your for loop, push the iterator to your array. Then outside the for
 * loop, return "tempDeck" as below:
 *
 *    function generateDeck(){
 *      var tempDeck = [];
 *
 *      for(var i=1; i<=52; i+=1){
 *        tempDeck.push(i);
 *      }
 *
 *      return tempDeck;
 *    }
 *
 * Outside your function, change your call to "generateDeck" as follows, being
 * sure to log your new deck variable to console:
 *
 *    var deck = generateDeck();
 *    console.log("My new deck is:",deck);
 *
 * Run the program via terminal/command prompt via command:
 *
 *    node <filename.js>
 *
 * where a list of numbers from 1-52, inclusive and ordered, should show! Your
 * deck is good to go!
 *
 * Now, let's think about the shuffle algorithm. Two key pieces of insight
 * should provide us the insight we need to begin. First, note that for our
 * "deck" of cards, each card (each number) is unique. That is to say, for any
 * given card in our deck, there exists only one of its exact kind in the deck.
 * This means likewise that the index at which that card occurs is the *only*
 * index at which that card will occur. Then what if we were not to directly
 * swap cards, but swap using each card's index?
 *
 * Let's try this approach. Create a function called "shuffle" that takes a
 * single argument, "deckIn" (the deck which is to be shuffled). Note that,
 * because this function must modify data outside itself, it is not "pure".
 * Before we begin building up our function, let's first add a console log
 * statement inside:
 *
 *    function shuffle(deckIn){
 *      console.log("Hello from Shuffle!",deckIn);
 *    }
 *
 * Go ahead and delete all other console log statements in the program, then
 * (outside your function), call shuffle, passing your "deck" variable to the
 * functions argument:
 *
 *    shuffle(deck);
 *
 * Run your program via terminal/command prompt via Node:
 *
 *    node <filename.js>
 *
 * If your message and deck again print to terminal/ command prompt, you're
 * good to go to the next step!
 *
 * If we are to shuffle our deck, it should make intuitive sense that we ought
 * shuffle every card. Then a for loop should appear a natural way to go about
 * ensuring this. Remove the console log statement from your function and
 * add your for loop. Note that, since we are iterating over the *array* of
 * cards, your iterator should start at zero, conditonal should check if the
 * value of the iterator is less than the length of "deckIn", and iterator
 * should increment by one:
 *
 *    function shuffle(deckIn){
 *      for(var i=0; i<deckIn.length; i+=1){
 *        console.log("value of iterator is:", i);
 *      }
 *    }
 *
 * Add the console log statement inside your for loop as well, then again run
 * the program from terminal/command prompt. Instead of numbers from 1-52
 * (inclusive), numbers from 0-51 should appear in sequential order.
 *
 * Fantastic! Now remove the console log statment and let's implement the "guts"
 * of our shuffle. Recall that we want to swap via a card's indicies. Likewise,
 * given that the index at which a card occurs is the first (and only) index
 * at which that card occurs, the current value of the iterator seems like a
 * starting point for the index of our current card. Likewise, we can generate
 * the swap index by generating a random integer between zero and the length
 * of our array (i.e. from zero to array.length, non-inclusive).
 * Add the following lines to your function below:
 *
 *    function shuffle(deckIn){
 *      for(var i=0; i<deckIn.length; i+=1){
 *        var swapIndex = Math.floor(Math.random() * deckIn.length);
 *      }
 *    }
 *
 *
 * Now that we've generated our swap index, let's swap the cards in our deck!
 * This can be done as follows:
 *
 *    function shuffle(){
 *      for(var i=0; i<deckIn.length; i+=1){
 *        var swapIndex = Math.floor(Math.random() * deckIn.length);
 *
 *        var tempCard = deckIn[i];
 *        deckIn[i] = deckIn[swapIndex];
 *        deckIn[swapIndex] = tempCard;
 *      }
 *
 *      console.log("SHUFFLED DECK:",deckIn);
 *      console.log("CHECK SHUFFLED DECK LENGTH:",deckIn.length);
 *
 *      return deckIn;
 *    }
 *
 * If you haven't seen a swap like this before (or at all), let's take a moment
 * to explain the logic behind the code above. First we assign the card
 * at the iterator's current value to a temporary variable for safe-keeping.
 * We then overwrite the card at this index in our deck with the card at the
 * swap index. Finally, we overwrite the card at the swap index with the
 * original card that was stored in our temporary variable! We've now completed
 * our shuffle!
 *
 * Note the addition of two console log statements above. In order to ensure
 * that our shuffle worked, we need to verify that the values stored in our
 * deck have been swapped correctly. There are several criteria to examine then:
 *
 *    1.) All values in the deck remain their original values (i.e. no incorrect
 *        values or erroneous data has been inserted by the swap).
 *
 *    2.) The length of the swapped deck is fifty-two elements
 *
 *    3.) The deck contains 52 *unique* elements, no duplicates have been
 *        induced by the swap.
 *
 * The console log statements above ensure that the former two of these
 * requirements are met. Run your program again in terminal/command prompt
 * via Node. Your program should output both log statments, the first displaying
 * your array with all numbers (1-52 inclusive), and the second denoting that
 * the length of your swapped array remains fifty-two.
 *
 * In order to verify our third requirement is met, we must now affect the
 * following changes *outside* our function:
 *
 *    deck = generateDeck(deck);
 *
 *    var checkLen = 0;
 *    deck.forEach(function(card,index){
 *      if(deck.indexOf(card) === index){
 *        checkLen += 1;
 *      }
 *    });
 *
 *    console.log("There are:",checkLen,"unique elements in your array.");
 *
 * The first line is a modification to our call to our shuffle function,
 * assigning the shuffled deck back to the our outside deck variable. We then
 * utilize the "forEach" method to iterator over each element in our shuffled
 * array. The conditional inside the method uses "indexOf" to determine if any
 * duplicates are present. Recall from our previous exercises that "indexOf"
 * returns only the *first* location/position at which an element in an array
 * appears. If the index of the card being iterated over is *not* the first
 * index at which it appears, that card is then a duplicate! If the conditional
 * returns true, we then increment a counter variable that keeps track of how
 * many unique elements are in the array.
 *
 * Go ahead and run your program via termina/command prompt and Node again.
 * Note that your console log statement should indicate that there are
 * fifty-two unique elements in your shuffled array. The shuffle algorithm is
 * a success! Congrats!
 */

function generateDeck(){

  var tempDeck = [];

  for(var i=1; i<=52; i+=1){
    tempDeck.push(i);
  }
  return tempDeck;
}


function deckMap(deckIn){
  for(var i=0; i<deckIn.length; i+=1){

    var card = '';

    if(deckIn[i] <= 13){
      deckIn[i] = deckIn[i] + 'H';
    }
    else if(deckIn[i] <= 26){
      deckIn[i] = (deckIn[i]%13 !== 0) ? (deckIn[i]%13) +'S' : '13S';
    }
    else if(deckIn[i] <= 39){
      deckIn[i] = (deckIn[i]%13 !== 0) ? (deckIn[i]%13) + 'C' : '13C';
    }
    else if(deckIn[i] <= 52){
      deckIn[i] = (deckIn[i]%13 !== 0) ? (deckIn[i]%13) + 'D' : '13D';
    }
    else{
      console.log("Too many cards!");
    }

    return deckIn;
  }
}

var deck = generateDeck();

deck = deckMap(deck);
console.log("Mapped deck:",deck);

function shuffle(deckIn){

  for(var j=0;j<deckIn.length;j+=1){
    var swapIndex = Math.floor(Math.random() * deckIn.length);

    var tempCard = deckIn[j];
    deckIn[j] = deckIn[swapIndex];
    deckIn[swapIndex] = tempCard;
  }
  // console.log("DECK IS",deckIn)
  // console.log("DECK LENGTH",deckIn.length);
  return deckIn;
}

deck = shuffle(deck);

var checkLen = 0;
deck.forEach(function(card,index){
  if(deck.indexOf(card) === index){
    checkLen += 1;
  }
});

// console.log("There are",checkLen,"unique elements in your array.");
