//
// EXERCISE: Construction Work
//
// We're going to simulate a 
//

function Customer(name, balance, debits, credits){
  this.name = name;
  this.balance = balance;
  this.debits = debits;
  this.credits = credits;

  var self = this;
  self.spend = function spend(amount){
    self.balance -= amount;
    self.debits += amount;
  }
  self.deposit = function deposit(amount){
    self.balance += amount;
    self.credits += amount;
  }
  self.accountStatement = function accountStatement(){
    return "BALANCE: "+String(self.balance)+", DEBITS: "+String(self.debits)+", CREDITS: "+String(self.credits)
  }
}

var John = new Customer("John",2300,0,0);

John.deposit(1300);
console.log(John.accountStatement());
John.spend(1500);
console.log(John.accountStatement());
