/*
 * EXERCISE: Power Funk
 *
 * If you're starting this exercise, be sure to follow the directions in
 * function.js first and complete all steps there! Once done, come back here
 * and we'll continue.
 *
 * Now that you've completed and exported your power function, let's require
 * it in like so:
 *
 *    var functions = require('./function');
 *
 * Go ahead and then log functions to console below, and run this program via
 * Node:
 *
 *    node main.js
 *
 * But wait a second! Our function is buried in things we don't want! When you
 * export a function via "module exports" and use dot notation, you must access
 * the export via that dot notation in the file in which it is imported. Then
 * modify your console log statement as below:
 *
 *    console.log(functions.power)
 *
 * Then run the program again:
 *
 *    node main.js
 *
 * Sure enough, our function now shows in our terminal/command prompt! Now,
 * let's modify our console log statement as to actually call the function:
 *
 *   console.log(functions.power(2,2));
 *
 * Then run the file again using the Node command above in your terminal/command
 * prompt. Now the number four prints to our console! We now know how to access
 * our imported "power" function, but it'd be nice if we didn't have to call
 * "functions.power()" every time we wanted to use it. Go ahead and remove your
 * console log statement. Instead, assign a variable called "power" to your
 * imported "power" function as below:
 *
 *    var power = functions.power;
 *
 * Then, call power as below, passing in a number and to what power you wish
 * to raise it (as well as logging the call to console):
 *
 *    console.log(power(2,2));
 *
 * Then run the program again via:
 *
 *    node main.js
 *
 * command in your terminal/console. Our function still works! We can still
 * improve though. Delete your new "power" variable, and instead rename your
 * "functions" variable to "power". Then, modify your require statement as
 * below:
 *
 *    var power = require('/function.js').power;
 *
 * Run the program again via Node from terminal/console. Yet again, note that
 * our function still works. However, our code is much more consise. When
 * importing functionality from larger libraries, you'll often use the above
 * method of import, as it ensure that you only import the required function
 * or behavior as opposed to the library of functions that might be contained
 * within a single export! Good job!
 */

 var power = require('./function.js').power;

 console.log(power(2,2));
