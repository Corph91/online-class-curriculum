/*
 * EXERCISE: Power Funk
 *
 * Just to get more practice with our Node import/export we're going to create
 * a function that will generate powers of the input number! Below, your misson
 * should you choose to accept it, is to create a function, "power" that
 * accepts two arguments, "numIn" the number which is being raised to a certain
 * power, and "powerIn", the power to which "numIn" is being raised! The
 * function should return numIn. To build your function, I recommend using a
 * for loop (inside the function but before return statement) that iterates
 * powerIn number of times like so:
 *
 *    function power(numIn, powerIn){
 *      for(var i=0; i<powerIn-1; i+=1){
 *        ...code in here...
 *      }
 *      return numIn;
 *    }
 *
 * Once you've written your function, call it down below, logging the call to
 * console and passing a few test parameters like this:
 *
 *    console.log(power(2,3))
 *
 * Test the call by running this file via Node as below:
 *
 *    node function.js
 *
 * If your test run outputs a valid power of the input number correctly, your
 * function is good to go!
 *
 * Now that we've tested our function, delete the console log statement, and
 * assigning it to Node's "module.exports" as follows:
 *
 *      module.exports.power = power;
 *
 * Note that we're using dot notation to assign power to a specific property of
 * exports. That's right! Like so much of JavaScript, exports is an object!
 * Ususally it's a good idea to add a specific key when exporting a given
 * function in Node, mainly for clarity and to give exports a logical (and
 * coherent) structure. Go ahead and move on to main.js!
 */

 function power(numIn, powerIn){
   for(var i=0; i<powerIn-1; i+=1){
     numIn *= numIn
   }
   return numIn;
 }

 module.exports.power = power;
