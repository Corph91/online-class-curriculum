/*
 * EXERCISE: Splicing Nodes
 *
 * So far I've mostly had you working with arrays of data. This might seem
 * incredibly repetitious, but they really do make up a huge part of programs!
 * Today, I'm going to introduce you to a cool way we can use arrays as values
 * objects to represent a "graph" data structure! A graph data structure works
 * on the basis of "nodes" (think of nodes as an intersection of streets) and
 * "edges" (an individual street). Graphs are one of *the* most useful data
 * structures (means of storing data) in computer science! Below, you'll see
 * one way to represent a graph, called an "adjacency list". An adjacency list
 * basically contains a set of key-value pairs. A key represents a given "node"
 * in the graph, while it's paired array contains strings representing
 * neighboring nodes to that given node. For example, if node A's neighboring
 * nodes (nodes connected directly to that node by a single edge) are nodes
 * "B" and "C", then "A" would appear in an adjacency list as:
 *
 *  {"A": ["B". "C"]}
 *
 * Our graph below then looks like:
 *
 *
 *         A
 *        /| \
 *      /  |  \
 *     B   C   D
 *     |\  |  /|
 *     | \ | / |
 *     E   F   G
 *     \   |  /
 *      \  | /
 *       \ |/
 *         H
 *
 * Note that for both sake of ease and convenience, we'll only be working with
 * an "undirected" graph (think of an "undirected graph" as being a city with
 * no one-way streets).
 *
 * Your goal here is to remove all edges from a given "layer" (i.e. A, B-C-D,
 * E-F-G,H) of the graph for which there exists more than one edge in a layer
 * from that layer to a given node in the next layer. For example, nodes B, C,
 * and D all have an edge from themselves to node F. You goal is to ensure that
 * ONLY either node B,C, or D has an edge to node F. However, you would not
 * want to remove any edges from node A to B,C, or D, nor from B to E, or D to
 * G!
 *
 * One thing that might occur to us in this problem is that we need to remove
 * duplicate nodes from a given layer. Again, let's break this problem into
 * smaller sub-problems. First, let's create a way to identify a layer! Note
 * that, for a layer, no nodes in the same layer have edges to one another.
 * then in order to identify a layer, let's loop over each key in our array,
 * and check whether it's associated array of neighbors contains an edge to
 * the preceding and next key. This can be done with careful configuration of
 * a for loop like so:
 *
 *    for(node in graph){
 *      var node = []
 *    }
 */

 var graph = {A: ["B","C","D"],
              B: ["A","E","F"],
              C:["A","F"],
              D: ["A","F","G"],
              E: ["B","H"],
              F: ["C","D","E","H"],
              G: ["D","H"],
              H: ["E","F","G"]}
