/*
 * EXERCISE 3: Basic Node Practice
 *
 * Welcome to week three! Today we're going to introduce the sever-side.
 * JavaScript runtime NodeJS! Node is a powerful tool, allowing us to run
 * JavaScript on both front-end (what the user sees) and backend (what the
 * developer/sys-admin/server sees) of our applications! Once we've walked you
 * through successfully installing NodeJS, start here!
 *
 * Below, write a console log statement saying "Hello World!" (or some rough
 * equivalent). Then, open up your terminal/command prompt and navigate to
 * the directory in which this file is. To run most JavaScript files via Node,
 * you'll want to usually be in the directory in which that file is stored.
 * Now, type the following command in your terminal/command prompt window:
 *
 *    node main.js
 *
 * Inside your terminal, note that your console logging statement's message now
 * prints to your terminal/command prompt shell!
 *
 * Let's do something even more incredible. Inside this file, write a function
 * called "counter" that takes no arguments and has no return statement. Inside
 * this function, write a traditional for loop that iterates at least ten times.
 * Inside the for loop, write a console log statement that logs the value of the
 * iterator to console. Then, in your termina/command prompt (after saving this
 * file and calling your "counter" function below), type the command:
 *
 *   node main.js
 *
 * and hit enter. Note that your console log statement from within your counter
 * function works as well! Now, modify your function so that the console log
 * statment is encapsulated by an if-statement. This if-statement should check
 * if the value of the iterator is evenly divisible by two, and only execute the
 * console log statement if this condition is true. Recall that you can check
 * the divisiblity of a number easily via the modulo operator as below:
 *
 *    if(i%2 == 0){
 *      console.log("PRINTING NUMBER",i);
 *    }
 *
 * One more time, save your file and run the following command:
 *
 *    node main.js
 *
 * If only even numbers in the range you specified for your for loop log to your
 * terminal/command prompt shell, you've completed this challenge!
 */

function counter(){
  for(var i=1; i<=12; i+=1){
    if(i%2 == 0){
      console.log(i,"is evenly divisible by 2!");
    }
  }
}

counter();
