// BANK CUSTOMER AND CONSTRUCTOR FUNCTIONS
// The following exercise fully introduces ES6 classes, constructors, and OO
// programming concepts.

var Account = class Account {

  constructor(balance,type){
    // DATA
    this.balance = balance
    this.debits = 0
    this.credits = 0
    this.type = type
  }


  // METHODS
  deposit(amount){
    this.balance += amount
    this.credits += amount
    console.log("Deposited",amount,"from",this.type,"account. Balance is now:",this.balance)
  }

  withdraw(amount){
    this.balance -= amount
    this.debits -= amount
    console.log("Withdrew",amount,"from",this.type,"account. Balance is now:",this.balance)
  }

  statement(){
    console.log(this.type,"account has a balance of:",this.balance,"dollars with:",this.credits,"in deposits and",this.debits,"in withdrawls to date")
  }
}


var Customer = class Customer{

  constructor(initChecking, initSavings){
    // ACCOUNTS
    this.checking = new Account(initChecking, "Checking")
    this.savings = new Account(initSavings, "Savings")
  }

  // METHODS
  getFullStatement(){
    console.log("")
    console.log("/////////////////////////////////////////////////////////")
    console.log("Checking account:")
    this.checking.statement()
    console.log("")
    console.log("Savings account:")
    this.savings.statement()
    console.log("/////////////////////////////////////////////////////////")
    console.log("")
  }
}

const Greg = new Customer(5000,2500)
console.log(Greg)
Greg.checking.deposit(2000)
Greg.checking.statement()
