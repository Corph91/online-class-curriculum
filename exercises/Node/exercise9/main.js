/*
 * EXERCISE: Roll the Dice
 *
 * Now that' we've had our first brushes with Node, let's create a simple
 * dice game! When modeling games, it pays to think about what the components
 * of the game are, and dice is no different. First, we should have two dice,
 * and a way of generating a "roll" of said dice. Next, we'll need some way
 * of checking our roll to see if it it's a 7 or 11, then return whether we've
 * won or not! Breaking down a general idea into its individual parts is a good
 * way to start thinking about how to build a program. In doing so, you can
 * begin to think of ways to "descritize", to encapsulate a given piece of the
 * required functionality (often using functions). Let's embrace and demonstrate
 * this methodology.
 *
 * Generating two random "dice rolls" sounds like a job for Math.random()!
 * Recall that you can generate random whole numbers in a given range (n) as
 * follows:
 *
 *    Math.floor(Math.random() * n + offset)
 *
 * For example, to generate random whole numbers 1-10, I would write:
 *
 *    Math.floor(Math.random() * 10 + 1);
 *
 * Let's start by writing a function called "roll". Roll should take no
 * arguments, and return two separate calls to our random number generator.
 * Recall that JavaScript can't return multiple values from a function. However,
 * we *could* wrap our two random generator calls in an object and return that:
 *
 *    function roll(){
 *      return {d1: Math.floor(Math.random() * 6 + 1),
 *              d2: Math.floor(Math.random() * 6 + 1)}
 *    }
 *
 * Go ahead and call the function below, logging the call to console, then run
 * the program via termina/command prompt with:
 *
 *    node main.js
 *
 * If an object containing two numbers appears, you're good to go to the next
 * step!
 *
 * You'll note that our function above doesn't contain incredibly complex or
 * detailed code. In using and creating functions, it often pays to keep the
 * code inside a function focused on a individual behavior. Then, if this is
 * our random dice roll, we ought encapsulate our checking inside a function
 * as well. First, delete your console log statement and call to your "roll"
 * function, then create a function called "checker" that takes no arguments.
 * Inside call your "roll" function, assigning it to a new variable "newRoll".
 * Log "newRoll" to console below the assignment, and then call your new
 * "checker" function:
 *
 *    function checker(){
 *      var newRoll = roll();
 *      console.log("NEW ROLL",newRoll)
 *    }
 *
 *    checker()
 *
 * Run your program via terminal/command prompt via the command:
 *
 *    node main.js
 *
 * If your object containing two random numbers logs to console, you're Good
 * to go to the next step!
 *
 * Remove the console log statement inside your "checker" function, and instead
 * add an empty if/else block as below:
 *
 *    function checker(){
 *      var newRoll = roll()
 *      if(){
 *
 *      }
 *      else{
 *
 *      }
 *    }
 *
 * Inside the conditional of the if statment, check if the sum of "newRoll.d1"
 * and "newRoll.d2" is strictly equal to seven or eleven:
 *
 *    function checker(){
 *      var newRoll = roll();
 *      if((newRoll.d1 + newRoll.d2) == 7 || (newRoll.d1 + newRoll.d2) == 11){
 *
 *      }
 *      else{
 *
 *      }
 *    }
 *
 * Inside the if statment, add a console log statment stating that the user has
 * won, and inside the else statement add a console log statement stating that
 * the user has lost. Then, run your program again in your termina/command
 * prompt:
 *
 *    node main.js
 *
 * Go ahead and play your game a few times! Aweseome work!
 */

 function roll(){
   return {d1: Math.floor(Math.random() * 6 + 1),
            d2: Math.floor(Math.random() * 6 + 1)};
 }

 function checker(){
   var newRoll = roll();
   if((newRoll.d1 + newRoll.d2) === 7 || (newRoll.d1 + newRoll.d2) === 11){
     console.log("I win!",newRoll.d1,newRoll.d2);
   }
   else{
     console.log("I didn't win!",newRoll.d1,newRoll.d2);
   }
 }

 checker();
