/*
 * EXERCISE: Objectivity
 *
 * We haven't touched objects in a while, so let's take a moment to review
 * some of the basics. Our goal today is to create a "graph" data structure.
 * Think of a graph like a network of streets and intersections. A intersection
 * represents a "node", a meeting of two or more streets. Likewise, an "edge"
 * is a street that connects two "nodes". Graphs are useful data structure due
 * to the efficiency with which one can conduct search, insertion, and deletion
 * operations. Let's give it a shot!
 *
 * Below, I've created an empty object representing our graph. Let's start by
 * logging this to console, then running our program vai terminal/command prompt
 * via Node! If an empty object shows, you're good to start!
 *
 * We've done this once before, but objects can store far more than data. We're
 * going to create a parameter "addNode" to our object, setting it to a function
 * of like name as below:
 *
 *    graph.addNode = function addNode(label){
 *      this[label] = [];
 *    }
 *
 * Note the use of "this". In the case of the above code, "this" refers to our
 * "graph" object. The function works by taking a "label" argument and passing
 * it to our graph, creating a new parameter whose key value is set to the
 * label, and whose value is an empty array. Go ahead and try it as below:
 *
 *    graph.addNode("A");
 *    console.log(graph);
 *
 * Run your program again via Node. Note that your graph object now contains
 * two parameters. One set to our "addNode" function, and the other our "A" node
 * set to an empty array! Now, go ahead and add several more nodes!
 *
 * Of course, just adding nodes is pretty boring. What if we want to delete them
 * too? JavaScript allows for an elegant solution using the keywords "delete"
 * and "this". Let's go ahead and create our "removeNode" function as below.
 * This function should accept a single argument (a String type indicating
 * they key value of the node to be removed):
 *
 *    graph.removeNode = function removeNode(node){
 *      delete this[node];
 *    }
 *
 * Again, call the function and delete one of the nodes you've previously
 * added:
 *
 *    graph.removeNode("D");
 *
 * Then log the graph to console again:
 *
 *    conosle.log("After removing node:",graph);
 *
 * Note that the parameter you deleted no longer appears. Fantastic! Let's go
 * even deeper. So far, nodes in our graph only maintains a list of that node's
 * neighbors. In practice, you'll usually want to store some sort of data at
 * each node! To enable our graph to do this, we need to start by modifying our
 * "addNode" function as below:
 *
 *  graph.addNode = function addNode(label){
 *    this[label] = {neighbors: [], data: undefined}
 *  }
 *
 * Note the use of the JavaScript keyword "undefined". This is a safe
 * placeholder for now. We now need to create a function to set the data stored
 * by a specfic node. Note that we can do so as below:
 *
 *   graph.setNode = function setNode(node,dataIn){
 *      this[node].data = dataIn
 *    }
 *
 * Our function above requires two arguments; the first is a String indicating
 * which node we are adding data to, and the second is the data we pass to be
 * stored at the node selected. Go ahead and call your new "setNode" function,
 * passing it some data and then logging your graph to console:
 *
 *    graph.setNode("A","Hello world!");
 *    console.log("Graph is now",graph);
 *
 * Run your program via Node. Excellent! Three more pieces of functionality
 * remain. Our graph must be able to:
 *
 *    1.) Access the data of a node.
 *    2.) Make two nodes neighbors
 *    3.) Remove a node that is a neighbor of another.
 *
 * The first requirement can be met by creating a new function "getNode" that is
 * a simple modifcation of our "setNode" function. Unlike "setNode", "getNode"
 * should only take a single argument (a String representing the node we want
 * to access) and should return the data stored at that node. This can be done
 * as follows:
 *
 *    graph.getNode = function getNode(node){
 *        return this[node].data;
 *    }
 *
 * Go ahead and call the "getNode" function on a node you've added to your
 * graph, logging the function call to console as below:
 *
 *    console.log("The data at node A is",graph.getNode("A"));
 *
 * Then run your program via Node. This time, instead of the entire object
 * printing to terminal/command prompt, the data at the node selected shows!
 *
 * Now to implement the final two pieces of functionality, the ability to add
 * and remove a one node as the neighbor of another. To start, let's address
 * the presiding question of "why would I even want to do this"? The proximity
 * of one node to another can be useful in indicating similarity,
 */

 var graph = {};
 console.log("Graph is",graph);

 graph.addNode = function addNode(label){
   this[label] = {neighbors: [], data: undefined};
 }

 graph.addNode("A");
 graph.addNode("B");
 graph.addNode("C");
 graph.addNode("D");

 graph.removeNode = function removeNode(node){
   delete this[node];
 }

 graph.addNeighbor = function addNeighbor(node,neighbor){
  if(Object.keys(this).indexOf(node) !== -1 && Object.keys(this).indexOf(neighbor) !== -1){
    this[node].neighbors.push(neighbor)
    this[neighbor].neighbors.push(node)
    return;
  }
  console.log("NODE DOES NOT EXIST");
 }

 graph.addNeighbor("A","B");
 graph.addNeighbor("A","C");
 graph.addNeighbor("A","D");
 console.log("Graph is now:",graph);

 graph.removeNeighbor = function removeNeighbor(node, neighbor){
   if(Object.keys(this).indexOf(node) !== -1 && this[node].indexOf(neighbor) !== -1){
     this[node].neighbors.splice(this[node].indexOf(neighbor), 1);
     this[neighbor].neighbors.splice(this[neighbor].indexOf(node),1);
     return;
   }
   console.log("NODE DOES NOT EXIST");
 }

 graph.findNode = function findNode(node){
   return (Object.keys(this).indexOf(node) !== -1) ? {node: node, neighbors: this[node].neighbors, data: this[node].data} : "N/A";
 }

 graph.findNeighbor = function findNeighbor(node,neighbor){
   if(Object.keys(this).indexOf(node) !== -1 && this[node].neighbors.indexOf(neighbor) !== -1){
     return {node: neighbor, neighbors: this[neighbor]}
   }
   return "N/A"
 }

 graph.createLayer = function createLayer(layer,nodes,rank){
   this[layer] = {rank};
   for(node in nodes){
     current = nodes[node];
     this[layer][current] = this[current];
   }
 }

 graph.createLayer("Input",["A"],1);
 graph.createLayer("H1",["B","C","D"],2);
 console.log(graph);
