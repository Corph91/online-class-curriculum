/*
 * EXERCISE 5: Importing and exporting node modules
 *
 * BEFORE YOU BEGIN: This exercise requires you to install
 * node in the exercise directory. So first run:
 *
 *     npm init
 *     npm install
 *
 * Let's get a little insight into how Node works with
 * modules. So far we've just been installing pre-built
 * npm packages, and bringing them in to our project.
 * The challenge I pose to you is:
 *
 * 1.) There is a function called "mergeSort()" in the "functions.js"
 *     file. Without copying and pasting the function into "main.js"
 *     (pretend this is a super secret algorithm and we don't want
 *     anyone to directly see the code), figure out how to access
 *     the function in "main.js" and use it to sort the array below.
 *
 * 2.) Once you've figured that out, create a new file called
 *     "search.js" (make sure it's in the same directory as main.js).
 *     In this file, write a function to find the median (middle) value
 *     of a sorted array, and then (without directly copying the code
 *     into "main.js"), figure out how to access the function in "main.js"
 *     and use it on our sorted array from step one.
 */
var functions = require('./functions');
var array = [];

for(i=0; i<100; i++){
  array.push(Math.floor(Math.random() * (100-0) + 1));
}

console.log("Here is our starting array:",array);

var result = functions.mergeSort(array);

console.log("Here's the result:",result);
