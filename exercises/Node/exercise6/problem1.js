/*
 * EXERCISE 6: Mastering export and require
 *
 * Node has a lot of subtle functionality, so getting familiar with the power
 * of export/import and its uses in developing applications is critical. Here,
 * I've created an express.js app for you to finish setting up. We're
 * going to take a look at one of the coolest ways to create routes. Your
 * mission is to take the route in this file, and bring it in to the
 * server.js file for use.
 */

var route = function(app){
  app.get('/testme',function(req,res){
    res.send("I'm here now!");
  });
}

module.exports.route = route;

// Using what functionality of node can we bring in our /testme route?
