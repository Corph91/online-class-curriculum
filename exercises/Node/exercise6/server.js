var express = require('express');
var app = express();
var port = 3000;

// Once you think you've successfully exported the function from problem1.js,
// let's take a crack at bringing it into our server.js file. How can we do
// this though, especially since our function takes an argument? Never fear!
//
// var problem1 = require('./problem1')('What goes in here?');
//
// If you follow the hint above, you'll get it in no time!
var pine = require('./problem1');

pine.route(app);

var server = app.listen(port,function(){
  console.log("Listening on port:",port);
});
