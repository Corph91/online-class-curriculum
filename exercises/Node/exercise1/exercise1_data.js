/*
 * EXERCISE 1: Welcome to Node!
 *
 * Welcome to week three! There's going to be a pretty big shift this week in
 * that we're moving from CodePen as an online environment to setting up our
 * local environment on our respective computers! If the last few weeks felt
 * like a lot of work, this where all that you've learned is going to start
 * paying off (not to say that it isn't already). By then end of this week,
 * you'll be building entire *apps* using a server-side JavaScrpt runtime
 * environment called NodeJS!
 *
 * The purpose of this exercise is to introduce import/export functionality
 * with Node, as well as reinforce some of the things we learned last week
 * (methods, integrated loops and conditionals). Start off by examining
 * your data: it's an array of objects! Each object is pretty simple, containing
 * only a user's name.  Our goal is to manipulate this data by adding both
 * "favoriteLanguage" and "favoriteApp" properties to each object, but (and
 * here's the catch) none of the manipulation will be done in this file.
 * Instead, we'll be using the Node method:
 *
 *    module.exports
 *
 * to pass our data to our main.js file, where we'll use "forEach" to loop
 * through the data, pushing it to an empty array in main.js! Your assignment
 * here is to call the module.exports method below our data, then assign it
 * to our data variable!
*/


var data = [{name: 'Travis T'},{name: 'Travis J'},{name: 'Doug'},{name: 'Sean'}, {name: 'Joseph'},
            {name: 'Hannah'}, {name:'Mike'}, {name: 'Emily'}, {name: 'Davis'}, {name: 'Jay'},{name: 'Devin'}]
