/*
 * EXERCISE 1: Welcome to Node!
 *
 * If you're starting this exercise, go ahead and open up exercise1_data.js
 * first and read the directions there. Once you've finished that part of the
 * assignment, come back here and we'll continue.
 *
 * In the former file, we've had you call a Node method called "module.exports"
 * and assign it to the data contained within that file. The purpose of this is
 * to illustrate a core functionality of Node: the development, export, and
 * import of Node JavaScript "modules" to encapsulate sever-side functionality
 * of a program by distributing pieces of a program (libraries, custom code,
 * even data) between seperate JavaScripy files as to preserve code readability
 * and make program development more modular. Recall how one use functions to
 * enhance the portability and reuse of code. Like reasoning applies to Node
 * modules. We import and export modules in Node to:
 *
 *  1.) Enhance functionality of existing code via libraries or other JavaScript
 *      resources.
 *
 * 2.) Maintain clear distinction between various components of a program,
 *     allowing for easier testing, debugging, and development.
 *
 * 3.) Allow for easier sharing and reuse of given code.
 *
 *
 * In our other file, we "exported" our data by setting the data equal to the
 * built-in Node method "module.exports". This is only half the battle though!
 * to access our exported data in this file, we need to *require* the data as
 * a part of this file's *dependencies*. Think of dependencies as required cogs
 * or gears for a bigger part of program's machinery to work. We can require
 * our data in as below:
 *
 *    var data = require('./exercise1_data')
 *
 * Note that:
 *
 *    1.) In requiring in an exported, custom module from another file or
 *        library we must be path-specific. This is important! In CodePen,
 *        we could generally assume that parts of a program we needed where
 *        already hooked together. This assumption can no longer hold. If you
 *        want to use exported data or functionality in another file, you need
 *        to require that file and make sure to pass the correct filepath as
 *        a string to Node's "require" method.
 *
 *    2.) We don't necessarily need to specify the type of our file in requiring
 *        it. Node automatically assumes that the type of file is a JavaScript
 *        file, so no need to add ".js" at the end.
 *
 * Now, below, require in your data then log your data variable to console.
 * In your terminal/command prompt, run:
 *
 *      node exercise1_main.js
 *
 * If all goes well, your array of objects should show in said terminal/command
 * prompt!
 *
 * Once you've ensured your data has been imported correctly, call "forEach" on
 * the array, and add two additional parameters "favoriteLanguage" and
 * "favoriteApp" to each object in the array, setting both new parameters equal
 * to empty strings (recall, any empty string can be either: '' or "").
 * After adding the parameters, still inside the "forEach" method, add each
 * object to the empty "newData" array below via the "push" method, then log
 * the "newData" array to console. Again, run your program in termina/command
 * prompt via the command shown above. If the objects printed to console in your
 * array show "favoriteLanguage" and "favoriteApp" properties, you did it!
 */

 var newData = [];
