/*
 * EXERCISE 2: Multiple Exports
 *
 * As with last exercise, if you're starting out this challenge open up and
 * complete the portion in data.js first! Once you're good to go there,
 * continue with the below.
 *
 * Last exercise, we introduced the idea of exporting Node "modules" from one
 * JavaScript file via "module.exports", and then importing exported modules
 * by "requiring" them in via a call to Node's "require()" method. This
 * challenge incorporates much the same, but there's a slight twist. Go ahead
 * and require in your exported modules from data.js into main.js as below:
 *
 *    var fromData = require('./data')
 *
 * Remember, when you require in module(s) from another file, the argument
 * passed to Node's "require()" method must be a string containing the *exact*
 * file-path to the file containing the modules we wish to import! Now, below
 * log your "fromData" variable to console, the run the following command in
 * your terminal/command prompt:
 *
 *    node main.js
 *
 * Unlike last exercise where our array of data printed to console, an object
 * containg our array and a function shows in our terminal/command prompt. What?
 * What if we only want one?! Is there some way we can access each item we
 * exported in our module independently? Recall a critical piece of insight,
 * that we set "module.exports" to an object where each parameter is named for
 * the given item we desired to export (and its associated value is the exported
 * item). Then couldn't we just use object dot notation to access our data and
 * function independently, assigning each to a variable locally? Try the below,
 * logging "importedData" and "searchUsers" to console as below:
 *
 *    var importedData = require('./data').data
 *    var searchUsers = require('./data').findUser
 *
 *    console.log("IMPORTED DATA:",importedData);
 *    console.log("IMPORTED FUNCTION:",searchUsers);
 *
 *  Run the following command in in your console:
 *
 *    node main.js
 *
 * Now, note that your array of objects and search function print to console
 * separately, no longer being properties of the object we wrapped them in so
 * we could export both in a single call to "module.exports"!
 *
 * For the final step, given that "searchUsers" now contains our "findUser"
 * function, call searchUsers below, passing "importedData" as the first
 * argument, and a valid string as the second as below:
 *
 *    console.log(searchUsers(importedData, 'some-string-here'));
 *
 * Be sure to pass a string (for the second argument) that you know will find
 * a user in our array of users. Run the program again from terminal/command
 * prompt with the command below:
 *
 *    node main.js
 *
 * If a user whose name contains the text you passed (as a string) to the second
 * argument of your "searchUsers" function prints to terminal/command prompt
 * shell, you've completed this challenge!
 */
