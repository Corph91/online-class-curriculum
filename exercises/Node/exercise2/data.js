/*
 * EXERCISE 2: Multiple exports
 *
 * In our last exercise, we only exported data out of our data.js file and then
 * did all our manipulation in the main.js file. The goal of this exercise
 * is demonstrate that:
 *
 *    1.) More than data can be exported via Node's "module.exports"
 *
 *    2.) You can export multiple items via "module.exports"
 *
 *
 * Your first task here is to write a function called "findUser". It should
 * accept two arguments, "dataIn" (an array of objects to search), and "query"
 * (a valid string). For now, our function will be simple, returning only the
 * first match found. You may use either a traditional for loop OR the "forEach"
 * method to iterate over the function's "dataIn" argument. Recall that if you
 * write a traditional for loop, your iterator should start at zero!
 *
 * The for loop or call to "forEach" should then be *inside* your "findUser"
 * function. Likewise, your for loop should contain an if-statement, calling
 * the "includes" method on each string of "name" parameter of a given user
 * being iterated over, checking to see if a name "includes" the string passed
 * to the function's "query" argument. If it does, then return that user. Your
 * function should then look something like this:
 *
 *    function findUser(dataIn, query){
 *      for(...){
 *        if(dataIn[i].name.includes(query)){
 *            return dataIn[i];
 *        }
 *      }
 *    }
 *
 * Note that, unlike previous exercises, I haven't provided you an entire
 * function to compare to or reference. The reason for this isn't that I'm
 * trying to be mean! This sort of "pseudo-code" is a common way programs are
 * written (by hand or otherwise) *before* being implemented in a given
 * language! Familiarizing yourself with how to read pseudo-code will really
 * help you implement much more complex algorithms or structures in the future!
 *
 * Now, call your function down below and log the call to console, passing it
 * the data variable for the first argument, and a valid string as the second
 * argument! Then, in your terminal/command prompt, run the following command:
 *
 *    node data.js
 *
 * If your function outputs a found object to console, you're good to go to the
 * next part!
 *
 * Now, let's make both our data *and* function available for use by our
 * "main.js" file! To do so, set "module.exports" as follows:
 *
 *    module.exports = {findUser: findUser, data: data }
 *
 * The above is one way you can export multiple functions/data from a given
 * JavaScript file via Node, by setting "module.exports" equal to an object,
 * where each parameter is key to a given function/piece of data we wish to
 * export!
 */

var data = [{name: 'Travis T', favoriteLanguage: '', favoriteApp: ''},{name: 'Travis J', favoriteLanguage: '', favoriteApp: ''},
            {name: 'Doug', favoriteLanguage: '', favoriteApp: ''},{name: 'Sean', favoriteLanguage: '', favoriteApp: ''},
            {name: 'Joseph', favoriteLanguage: '', favoriteApp: ''}, {name: 'Hannah', favoriteLanguage: '', favoriteApp: ''},
            {name:'Mike', favoriteLanguage: '', favoriteApp: ''}, {name: 'Emily', favoriteLanguage: '', favoriteApp: ''},
            {name: 'Davis', favoriteLanguage: '', favoriteApp: ''}, {name: 'Jay', favoriteLanguage: '', favoriteApp: ''},
            {name: 'Devin', favoriteLanguage: '', favoriteApp: ''}]
