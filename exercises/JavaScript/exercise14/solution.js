let deck = [];

const generateDeck = (deckIn,range) => {
  for(let i=1; i<=range; i+=1){
    deckIn.push(i);
  }
  return deckIn;
}

deck = generateDeck(deck,52);

const cardSuit = (cardIn) => {
  if(cardIn <= 13){
    return 'H'
  }
  else if (cardIn <= 26) {
    return 'D'
  }
  else if (cardIn <= 39) {
    return 'S'
  }
  else if (cardIn <= 52) {
    return 'C'
  }
  else{
    return 'Error: Invalid Suit'
  }
}

const cardScore = (cardIn) => {
  if(cardIn%13 < 10 && cardIn%13 > 0){
    return cardIn%13 + 1
  }
  else if(cardIn%13 === 10){
    return 'J'
  }
  else if (cardIn%13 === 11) {
    return 'Q'
  }
  else if (cardIn%13 === 12) {
    return 'K'
  }
  else if (cardIn%13 === 0) {
    return 'A'
  }
  else{
    return 'Error: Invalid Card Rank'
  }
}

const mapDeck = (deckIn) => {
  return deckIn.map((card) => {
    return cardScore(card) + cardSuit(card)
  })
}

deck = mapDeck(deck)


const shuffleDeck = (deckIn) => {
  deckIn.forEach((card,index) => {
    swapIndex = Math.floor(Math.random() * deckIn.length)
    deckIn[index] = deckIn[swapIndex]
    deckIn[swapIndex] = card
  })
  return deckIn
}

deck = shuffleDeck(deck)
console.log("Shuffled deck",deck)

const shuffleValidator = (deckIn,size) => {
  return deckIn.filter((card,index) => {
    return deckIn.indexOf(card) === index
  }).length === size
}

console.log("Deck is valid?:",shuffleValidator(deck,52))
