const accum = (s) => {
 let count = 1
 let output = ''

 for(let i=0; i<s.length; i+=1){
   output += s[i].toUpperCase()
   let repeat = 0
   while(repeat < i){
    output += s[i].toLowerCase()
    repeat += 1
   }
   if(i < s.length-1){
     output += '-'
   }

 }
	// your code
 return output
}
