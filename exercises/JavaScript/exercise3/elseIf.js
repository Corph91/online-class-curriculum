function grader(gradeIn){
    if(gradeIn >= 90){
        return 'A';
    }
    else if(gradeIn < 90 && gradeIn >= 80){
        return 'B';
    }
    else if(gradeIn < 80 && gradeIn >= 70){
        return 'C';
    }
    else if(gradeIn < 70 && gradeIn >= 60){
        return 'D';
    }
    else if(gradeIn < 60){
        return 'F';
    }

}

console.log("You got a:",grader(94));