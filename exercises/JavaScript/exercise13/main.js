// Step one: create an array that stores 100 blank arrays.


// Step two: fill each array with one-hundred random numbers of either 0 or 1.
// You must use map to iterate through each inner array inside the outer array.


// Step three: Chain together map and filter methods to reduce the array to
// an array of objects, with one key storing the index of any colums whose
// rows contain a non-zero entry, and the other key storing an array of non-zero
// row index values for its respective column.
