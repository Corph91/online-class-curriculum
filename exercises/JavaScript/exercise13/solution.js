let twoDimArray = []

// Init empty 2D array
for(let i=0; i<100; i+=1){
  twoDimArray.push([])
}

// Fill 2D array
twoDimArray = twoDimArray.map((subArray) => {
  for(let j=0; j<twoDimArray.length; j+=1){
    subArray.push(Math.floor(Math.random() * 2))
    if(Math.random() < 0.99){
      subArray[j] = 0;
    }
  }
  return subArray
})

console.log("Initial array",twoDimArray)

// Reduce 2D array
twoDimArray = twoDimArray.map((subArray, outer) => {
  positions = subArray.map((item, inner) => {
    return (item === 1) ? inner : null
  }).filter((item) => {
    return item !== null
  })
  return {col: outer, rows: positions}
}).filter((subArray) => {
  return subArray.rows.length > 0
})

console.log("Reduced array",twoDimArray)
