_____________________________________________
A basic exercise that creates a baseball simulation game
to help reinforce do-while, conditional/control structures,
and introduce Math.random():
https://codepen.io/Corpheus91/pen/NgKwQN

Solution here:
https://codepen.io/Corpheus91/pen/weweJL

_____________________________________________
In this exercise, we create a JavaScript clock,
with the goal of teaching JavaScript Date type
manipulation and reinforcing basic JavaScript DOM
manipulation concepts:
https://codepen.io/Corpheus91/pen/YQKEdd

Solution here:
https://codepen.io/Corpheus91/pen/Xgregr

_____________________________________________
This is a tutorial, reinforcing the Bootstrap,
DOM manipulation, and JavaScript concepts of
the past week by building a simple calculator
application:

https://codepen.io/Corpheus91/pen/RVXxrP
