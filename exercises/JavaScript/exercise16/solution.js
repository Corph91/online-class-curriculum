let sum = 0
let greatestNumber = 0
let lowestNumber = 0
let averageNumber = 0


// GENERATE DATA
const generateData = (size,range) => {
  let temp = []
  for(let i=0; i<size; i+=1){
    temp.push(Math.floor(Math.random() * range))
  }
  return temp
}

let array = generateData(1000,10000)


//SUM
const sumData = (arrayIn) => {
  let tempSum = 0
  arrayIn.forEach((num) => tempSum += num)
  return tempSum
}

sum = sumData(array)
console.log("Sum of all numbers is:",sum)


// MAXIMUM
const maximum = (arrayIn) => {
  let max = arrayIn[0]
  arrayIn.forEach((num) => { max = (num > max) ? num : max})

  return max
}

greatestNumber = maximum(array)
console.log("Max num in array is:",greatestNumber)


// MINIMUM
const minimum = (arrayIn) => {
  let min = arrayIn[0]
  arrayIn.forEach((num) => { min = (num < min) ? num : min })

  return min
}

lowestNumber = minimum(array)
console.log("Min num in array is:",lowestNumber)


//AVERAGE
const average = (arrayIn) => {
  let sum = 0
  arrayIn.forEach((num) => { sum += num })

  return sum/arrayIn.length
}

averageNumber = average(array)
console.log("Average of array is:",averageNumber)
