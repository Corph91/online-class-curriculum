Very simple, given a number, find its opposite.

Examples:

```
1: -1
14: -14
-34: 34
```

__But can you do it in 1 line of code and without any conditionals?__

credit: http://www.codewars.com/
