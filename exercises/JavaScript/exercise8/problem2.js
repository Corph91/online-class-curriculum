/*
 * Exercise 8: Encryptor
 *
 * If the last example of closures might have seemed toyish, this example
 * will make up for it. A key problem in Computer Science is data encryption,
 * particularly regarding user information that could result in unauthorized
 * access to a user's account, or other critical data (credit cards, address,
 * ect.). Our goal here is to build both an encrypt and decrypt function that
 * takes input from command line (using node), and returns both the encrypted
 * and decrypted input (for verification purposes).
 *
 * As you can probably guess, we don't want outside code (or outside
 * programmers) to have access to our super-secret encryption on decryption
 * function's encryption or decryption logic! Luckily we can use JavaScript
 * closures to prevent such access, all while enhancing code portability.
 *
 * Follow allong as I show you how to construct this program, and be sure
 * to ask lots of questions!
 */

var encryptDecrypt = function(){
    function getPhrase(){
        console.log("Enter your passphrase (please enter letters only):");
        var stdin = process.openStdin();
        stdin.addListener("data", function(input) {
            // note:  d is an object, and when converted to a string it will
            // end with a linefeed.  so we (rather crudely) account for that
            // with toString() and then trim()
            var inString = input.toString().trim();
            process.stdin.destroy();
            passphrase = inString.split("");

            passphrase = passphrase.filter(function(char){
                return !Number.isInteger(char);
            });

            passphrase = encryptor(passphrase);
            console.log("Your passphrase is:",passphrase);
            passphrase = decryptor(passphrase);
            console.log("Your decrypted passphrase is:",passphrase);
        });
    }

    function encryptor(pass){
         var keyData = {
            'A': 0,
            'B': 1,
            'C': 2,
            'D': 3,
            'E': 4,
            'F': 5,
            'G': 6,
            'H': 7,
            'I': 8,
            'J': 9,
            'K': 10,
            'L': 11,
            'M': 12,
            'N': 13,
            'O': 14,
            'P': 15,
            'Q': 16,
            'R': 17,
            'S': 18,
            'T': 19,
            'U': 20,
            'V': 21,
            'W': 22,
            'X': 23,
            'Y': 24,
            'Z': 25,
        };

        var keyArray = Object.keys(keyData);
        var result = [];


        pass.forEach(function(char){
            for(i=0; i<keyArray.length; i++){
                if(char === keyArray[i] || char === keyArray[i].toLowerCase()){
                    result.push(keyData[keyArray[i]]);
                }
            }
        });
        return result;
    }

    function decryptor(passIn){
        function keys(){
            var keyData = {
                '0': 'A',
                '1': 'B',
                '2': 'C',
                '3': 'D',
                '4': 'E',
                '5': 'F',
                '6': 'G',
                '7': 'H',
                '8': 'I',
                '9': 'J',
                '10': 'K',
                '11': 'L',
                '12': 'M',
                '13': 'N',
                '14': 'O',
                '15': 'P',
                '16': 'Q',
                '17': 'R',
                '18': 'S',
                '19': 'T',
                '20': 'U',
                '21': 'V',
                '22': 'W',
                '23': 'X',
                '24': 'Y',
                '25': 'Z',
                };

                var keyArray = Object.keys(keyData);
                var result = [];

                passIn.forEach(function(key){
                for(i=0; i<keyArray.length; i++){
                    if(key === Number(keyArray[i])){
                        result.push(keyData[keyArray[i]]);
                    }
                }
                });
                return result.join().replace(/,/g, '');
            }

            return keys();

        }

    getPhrase();
}

encryptDecrypt();
