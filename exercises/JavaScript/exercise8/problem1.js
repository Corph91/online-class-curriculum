/*
 * Exercise 8: JavaScript Cloures
 *
 * When coding in JavaScript, there are times we want an inner bit of
 * a function's "guts" to remain inaccessible to outside code. This prevents
 * other code outside a given function from tampering with its logic or
 * workings, preventing side effects (unintended behavior) in your code.
 * Follow along as I show you how to construct a function that returns the
 * square of some input number using JavaScript closure to prevent outside
 * access to the inner "square" function that actually does the mathematical
 * operation(s).
 */

var outside = function(a){

   function square(a){
       return a * a;
   }

   return square(a);
}


console.log("My square is",square(4));
