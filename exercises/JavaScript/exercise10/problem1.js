/*
 * Exercise 10: Sorting
 *
 * Being able to sort and divine order from often chaotic data is a
 * necessary skill for any programmer. In fact, it's so important that Travis
 * Wheeler (Assistant Chair of the CS Department at University of Montana)
 * told my entire Algorithms class that if we were ever at a lack of starting
 * points when building an algorithm, that sorting was the best place to begin.
 * I'm not going to throw anything terrible at you, but I'd like tot take the
 * time to both implement and cover Bubble Sort (an intuitive if inefficient
 * sorting algorithm), as well as show JavaScripts built-in sort method for
 * arrays. Follow along and ask questions!
 */

var array1 = [];
var array2 = [];

for(i=0; i<100; i++){
    array1[i] = Math.floor(Math.random() * (100-0) + 1);
    array2[i] = Math.floor(Math.random() * (200-0) + 1);
}

// Bubble Sort!
// var swapped;
// var newArray = [];
// do{
//     swapped = false;
//     for(j=1; j<array1.length; j++){
//         if(array1[j-1] > array1[j]){
//             var temp = array1[j-1];
//             array1[j-1] = array1[j];
//             array1[j] = temp;
//             swapped = true;
//         }
//     }

// }while(swapped);


array1 = array1.sort(function sortFunc(a,b){
    return a-b;
});

array2 = array2.sort(function sortFunc(a,b){
    return a-b;
});


array1 = array1.concat(array2).sort(function(a,b){
    return a-b;
});

console.log("Concatenated arrays",array1);
