// Create a directory in your warmumps or exercises folder called: basic-mathematical-operations
// Create main.js

console.log("Hello humans!")

// OPERATORS:
//
// Add: +
// Subtract: -
// Multiply: *
// Divide: /
// Modulo: %

// '+' or '-' or '*' or '/' or '%'

// basicOp('+', 4, 7)
// --> 11

// basicOp('-', 15, 18)
// --> -3

// basicOp('*', 5, 5)
// --> 25

// basicOp('/', 49, 7)
// --> 7

// basicOp('%', 20,10)
// --> 0

function basicMath(operator, num1, num2){
  var operator = operator
  var num1 = num1
  var num2 = num2

  if(operator === '+'){
    var result = num1 + num2
  }
  else if (operator === '-') {
    var result = num1 - num2
  }
  else if(operator === '*'){
    var result = num1 * num2
  }
  else if (operator === '/') {
    var result = num1 / num2
  }
  else if (operator === '%') {
    var result = num1 % num2
  }

  return result
}

//console.log("ADD",basicMath('+',4,7))
//console.log("SUBTRACT",basicMath('-',15,18))
//console.log("MULTIPLY",basicMath('*',5,5))
//console.log("DIVIDE",basicMath('/',49,7))
//console.log("MOD",basicMath('%',20,10))


// MATH OPS V2

function basicOp(op, num1, num2){
  if(op === '+'){
    return num1 + num2
  }
  else if (op === '-') {
    return num1 - num2
  }
  else if (op === '*') {
    return num1 * num2
  }
  else if (op === '/') {
    return num1 / num2
  }
  else if(op === '%'){
    return num1 % num2
  }
}


const betterOp = (op, num1, num2) => eval(num1 + op + num2)

console.log("ADD",betterOp('+',10,11))
console.log("SUBTRACT",betterOp('-',200,99))
console.log("MULTIPLY",betterOp('*',10,10))
console.log("DIVIDE",betterOp('/',5,2))
console.log("MOD",betterOp('%',8,6))
