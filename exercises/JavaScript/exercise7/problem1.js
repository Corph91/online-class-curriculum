/*
 * EXERCISE 7: String Theory
 *
 * So far we've been looking a lot at how to work with arrays
 * and objects, but knowing how to work with strings in javascript
 * is just as important (expecially when most of what you're going to
 * be working with will be text)! So lets' get our nerd on, and delve
 * into splicing, editing, and working with strings!
 */

 /*
  * PROBLEM 1a.) You assignment here is, giving the string below, to
  * break apart the string wherever a dash '-' occurs. For this part,
  * you may use a for loop, while loop, or whatever you please. Push each
  * part of the string (not including the dash) to the provided array, and
  * log the result to console.
  */

  var stringArray = [];
  var phoneNo = "987-654-3210";
  
  for(i=0; i<phoneNo.length; i++){
    var temp = '';
    while(phoneNo[i] != '-' && i < phoneNo.length){
      temp += phoneNo[i];
      i++;
    }
    stringArray.push(temp);
  }

  console.log("Our final result is:",stringArray);


 /*
  * PROBLEM 1b.) That was pretty easy, right? Now, like with the arrays, we're
  * going to explore some nifty methods strings have built-in. Remember,
  * underneath the hood, a method is just doing what you did above in some way!
  *
  * Your assignment is to again split the string above at the dash, but this
  * time without using any traditional loops (use a string method). Note that
  * you do not need to push the result to the array this time, the method we
  * seek will do it for you.
  *
  * HINT: The name of what we're doing, a 'split', should be a big hint as
  * to the method name we're looking for!
  */

<<<<<<< HEAD
  var newNumber = phoneNo.split('-'); // put method call here
=======
  var newNumber = phoneNo; // put method call here
>>>>>>> 9b382dda79ef68725823960f7d702762a71476af

  console.log("Our next result is:",newNumber);

 /*
  * PROBLEM 1c.) We have a big messy string and a message therein to find!
  * Your goal is to use built-in string functions to find the message "HELLO"
  * in our gibberish text. This is significantly more difficult to do without
  * using traditional loops, but with a little ingenuity you can do it!
  *
  * HINT: Some useful methods include:
  *
  * .search() : Searches string for specified value and returns pos. of match
  *
  * .includes() : Checks whether a string containes specified string/chars.
  *
  * .substring(): extracts characters from string between two specified indices.
  *
  * .match() : Searches a string for a match against an expression, returns
  *            matches. NOTE: this works for 'regular expressions' only, which
  *            are not normal strings. If you're prepared to dive into this,
  *            go ahead, but be sure to research the difference between normal
  *           strings and regular expressions.
  */

  var string = "ASDF32GH32io#UIOJ$ifnksJKJ#$JFD(nvr490398HELfEIUHGDS#&$()($*#DFSN948#*(AMVIROM#IPIDHELLODJIDJ$(FNi4jfn4934KHEL#O23098fdnLOHUIFN($*89320MC";
  var searchStr = "HELLO";
<<<<<<< HEAD
 
  var stringPos = string.search(searchStr);
  var stringEnd = stringPos + searchStr.length;
  console.log("Hello starts at",stringPos);

  var result = string.substring(stringPos, stringPos + searchStr.length);
=======

  var result = string;
>>>>>>> 9b382dda79ef68725823960f7d702762a71476af

  console.log(result,"there!");
