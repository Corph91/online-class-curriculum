const averageString = (str) => {
  // Code away
  let key = {"zero": 0, "one": 1, "two": 2, "three": 3, "four": 4,
             "five": 5, "six": 6, "seven": 7, "eight": 8, "nine": 9}
  let revKey = {0: "zero", 1: "one", 2: "two", 3: "three", 4: "four",
                5: "five", 6: "six", 7: "seven", 8: "eight", 9: "nine", NaN: "n/a"}

  let strArray = str.split(' ')
  let num = Math.floor(strArray.reduce((sum,strNum) => sum += key[strNum], 0)/strArray.length)

  return revKey[num]
}
