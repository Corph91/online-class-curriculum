Simple code pen demonstrating some core Bootstrap principles

CodePen
https://codepen.io/fresh5447/pen/OppbaB

Here is an additional intro to BS code pen
https://codepen.io/fresh5447/pen/QGQoBx

______________________________________________________
A more complex Bootstrap tutorial involving columns,
grid spacing, and inline JavaScript alerts on Codepen:
https://codepen.io/Corpheus91/pen/wewrWy

with the solution:
https://codepen.io/Corpheus91/pen/owvwEy
