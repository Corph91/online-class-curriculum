let names = ["David Bowie", "The Artist Formerly Known as Prince", "John Lennon", "Jimmi Hendrix", "Robert Plant", "Paul McCartney",
            "Gene Simmons", "Stevie Nicks", "Florence Welch", "Sonny and Cher"];

// Map the names to objects
// ex: {name: "David Bowie"}
// Return result to mappedNames array

// let mappedNamesES5 = names.map(function(name){
//   var rockstar = {name: name};
//   return rockstar;
// })
//
// mappedNamesES5 = "Hi!"
//
// console.log(mappedNamesES5);

const mappedNames = names.map((name) => {
  let rockstar = {name: name}
  return rockstar
})

//console.log(mappedNames)


// map through just the first names
// return an array of the first names of the artists

const mappedFirstNames = names.map(name => name.split(" ")[0])
///console.log(mappedFirstNames)


// Call filter method on names array.
// Only return artists names whose first names beginning with J.
// Store this result in a new array.

let filteredNamesJ = names.filter(name => name[0] === "J")
//console.log(filteredNamesJ)

// Chaining Methods
// a.methdOne().methodTwo()


// Return an array of first names of artists whose first names begin with "J"

let firstNameJ = names.filter(name => name[0] === "J").map(name => name.split(" ")[0])
///console.log(firstNameJ)


// SHAPES
let shapes = [
  { name: "Square", color: "Green", sides: 4 },
  { name: "Triangle", color: "Green", sides: 3 },
  { name: "Rectangle", color: "Red", sides: 4 },
  { name: "Pentagon", color: "Green", sides: 5 },
  { name: "Hexagon", color: "Red", sides: 6 }
]

// Call map on shapes array
// Return a new array of shape names

const shapeNames = shapes.map(shape => shape.name)
//console.log(shapeNames)

// Call filter on shapes array

const greenShapes = shapes.filter(shape => shape.color === "Green")
//console.log(greenShapes)

// Call filter on shapes array
// return new array of shapes whose have no. of sides < 4

const lessThanFourSides = shapes.filter(shape => shape.sides < 4)
console.log(lessThanFourSides)
