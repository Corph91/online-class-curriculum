const express = require('express')
const bodyParser = require('body-parser')
const app = express()
let port = 3000
let data = []

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// GET ALL
app.get('/',(req,res) => res.json({message: "Pulling all customer data.", data: data}))


// GET ONE
app.get('/:name',(req,res) => {
  let searchName = data.indexOf(req.params.name)

  if(searchName !==-1){
    res.json({message: "I found the customer!", data: data[searchName]})
  }
  else{
    res.json({message: "Oh no, that customer doesn't exist!", data: null})
  }
})


// ADD A NEW RECORD
app.post('/', (req,res) => {
  data.push(req.body.name)
  console.log(data)
  res.json({message: "Customer successfully submitted.", data: data})
})


// MODIFY AN EXISTING RECORD
app.put('/:customer',(req,res) => {
  let searchName = data.indexOf(req.params.customer)

  if(searchName !== -1){
    data[searchName] = req.body.newCustomer
    res.json({message: `Successfully modified customer ${req.params.customer}`, data: data})
  }
  else {
    res.json({message: "I couldn't find that customer!", data: null})
  }
})


// DELETE AN EXISTING RECORD
app.delete('/:customer',(req,res) => {
  let searchName = data.indexOf(req.params.customer)

  if(searchName !== -1){
    data.splice(searchName,1)
    res.json({message: "Customer successfully deleted!", data: data})
  }
  else{
    res.json({message: "I couldn't find that number!", data: null})
  }
})

app.listen(port,() => console.log("Listening on port:",port))
