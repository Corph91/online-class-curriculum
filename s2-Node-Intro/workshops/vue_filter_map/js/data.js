var title = "Baseball Players App"
var players = [
                {name: "Aaron Judge", average: '0.327', homeRuns: 27, rbis: 62, obps: '0.449', team: "NY Yankees"},
                {name: "Jose Altuve", average: '0.326', homeRuns: 11, rbis: 40, obps: '0.398', team: "Houston Astros"},
                {name: "Jose Ramirez", average: '0.325', homeRuns: 15, rbis: 42, obps: '0.379', team: "Clevland Indians"},
                {name: "Corey Dickerson", average: '0.321', homeRuns: 17, rbis: 40, obps: '0.363', team: "Tampa Bay Rays"},
                {name: "Carlos Correra", average: '0.319', homeRuns: 17, rbis: 58, obps: '0.394', team: "Houston Astros"},
                {name: "Buster Posey", average: '0.339', homeRuns: 10, rbis: 35, obps: '0.416', team: "SF Giants"},
                {name: "Ryan Zimmerman", average: '0.333', homeRuns: 14, rbis: 55, obps: '0.398', team: "Washington Nationals"},
                {name: "Daniel Murphy", average: '0.330', homeRuns: 9, rbis: 62, obps: '0.373', team: "Washington Nationals"},
                {name: "Zack Cozart", average: '0.322', homeRuns: 20, rbis: 33, obps: '0.403', team: "Cincinnati Reds"},
                {name: "Bryce Haper", average: '0.318', homeRuns: 24, rbis: 62, obps: '0.425', team: "Washington Nationals"},
                {name: "Logan Morrison", average: '0.256', homeRuns: 24, rbis: 57, obps: '0.366', team: "Tampa Bay Rays"},
                {name: "George Springer", average: '0.287', homeRuns: 23, rbis: 52, obps: '0.363', team: "Houston Astros"},
                {name: "Khris Davis", average: '0.255', homeRuns: 22, rbis: 62, obps: '0.341', team: "Oakland Athletics"},
                {name: "Mike Moustakas", average: '0.270', homeRuns: 24, rbis: 57, obps: '0.306', team: "Kansas City Royals"},
                {name: "Cody Bellinger", average: '0.260', homeRuns: 23, rbis: 52, obps: '0.332', team: "LA Dodgers"},
                {name: "Joey Votto", average: '0.316', homeRuns: 24, rbis: 57, obps: '0.428', team: "Cincinnati Reds"},
                {name: "Marcell Ozuna", average: '0.262', homeRuns: 17, rbis: 52, obps: '0.374', team: "Miami Marlins"},
                {name: "Giancario Stanton", average: '0.243', homeRuns: 14, rbis: 49, obps: '0.403', team: "Miami Marlins"},
                {name: "Eric Thames", average: '0.284', homeRuns: 20, rbis: 56, obps: '0.425', team: "Baltimore Brewers"},
                {name: "Robinson Cano", average: '0.287', homeRuns: 19, rbis: 61, obps: '0.366', team: "Seattle Mariners"},
                {name: "Nelson Cruz", average: '0.274', homeRuns: 18, rbis: 62, obps: '0.363', team: "Seattle Mariners"},
                {name: "Miguel Sano", average: '0.316', homeRuns: 20, rbis: 33, obps: '0.428', team: "Minnesota Twins"},
                {name: "Paul Goldschmidt", average: '0.282', homeRuns: 14, rbis: 57, obps: '0.353', team: "Arizona Diamondbacks"},
                {name: "Jake Lamb", average: '0.300', homeRuns: 18, rbis: 32, obps: '0.363', team: "Arizona Diamondbacks"},
                {name: "Nolan Arenado", average: '0.287', homeRuns: 15, rbis: 23, obps: '0.352', team: "Colorado Rockies"}
              ];

// Create a new array with only players name and home runs
// bonus: only do their first name
var nameAndHomeruns = players.map(function(player){
  return {name: player.name.split(" ")[0], homeRuns: player.homeRuns}
});

// 1) Create a new array of players with more than 23 homeruns
// 2) Map through your new array to return the players name, and homeruns
var homeRunsFilter = players.filter(function(player){
  return player.homeRuns > 23
}).map(function(player){
  return {name: player.name.split(" ")[0], homeRuns: player.homeRuns}
});

// Filter and map through the data to return all players that play for the Washington Nationals
var washingtonNationals = players.filter(function(player){
  return player.team === "Washington Nationals"
}).map(function(player){
  return {name: player.name, homeRuns: player.homeRuns}
});

// Use a for loop to get the average homeRuns num
function homeRunsAverage() {
  var total = 0;
  for (var i = 0; i < players.length; i++) {
    total += players[i].homeRuns
  }
  var average = total / players.length
  return average;
}

console.log(homeRunsAverage())
