# Var, Let, and Const

By transitioning to JavaScript ES6 syntax, we've gained two additional variable
declarations, *let* and *const*. Both of these function vastly differently than
*var*, and in many cases differently from each other. This workshop
demonstrates, through trial and error, the differences between these three
declarations.

### __Part One:__ Var

`var` is now the weakest signal available when you define a variable in
JavaScript. The variable may or may not be reassigned, and the variable may or
may not be used for an entire function, or just for the purpose of a block or
loop. `var` is what is called "function scope", meaning that the variable is
accessible within the `function` in which it is declared. When coding using
ES6 syntax (which you always should), `var` should almost *never* be used due
to the ambiguity associated with it.

We haven't declared things as `var` for a while, but let's go ahead and get some
practice. Create a `for-loop` in which all variables (the increment and
otherwise) are declared using `var`. Go ahead and then print the iterator to
console inside like so:

```
  for(var i=0; i<10; i+=1){
    console.log("The value of i is:",i)
  }
```

Then, outside the for loop, increment `i` again and log it to console once
more:

```
  i+=1
  console.log("The value of i is now:",i)
```

Go ahead and run the program. Note that the `for-loop` executes as we would
expect. Of concern though, note that `i` remains accessible outside the
`for-loop`! Clearly this could be dangerous, especially if we're creating
server-side code that could be passing sensitive information or systems-critical
functionality back or forth. Such opaque scope could easily result in values
being accidentally overwritten, improperly assigned, or simply "disappear".


### __Part Two:__ Const ###

In steps ES6 syntax with the `const` declaration. Variables declared as `const`
have several key differences that make them ideal for common usage:

* `const` variables are "block scope", meaning they are only accessible within
the nearest set of closing curly braces. Then for the example below, the value
`j` that we add to our increment is inaccessible to code outside the curlies of
the `for-loop`:

```
for(var i=0; i<10; i+=1){
  const j = 1;
  i +=j
  console.log("Value of i is:",i)
}
```

* `const` variables also cannot have their "identifiers" (variable names)
reassigned. This __does not__ mean that they are immutable. You can absolutely
change, increment, or otherwise modify the initial value assigned to a variable
declared as `const`. *However* you __cannot__ reassign a `const` variable's
initial value. This means that actions like using in-place operators to modify a
string or numeric value declared as a constant will result in an error as well,
as they involve reassignment. If we write and run the code below, we see that
an attempted reassignment of a `const` variable to a differing value beyond the
initial one results in a run-time error:

```
const name = "John Jefferies"
const nameArray = []

console.log("My name is:",name)
nameArray.push(name)
console.log("I am the name array:",nameArray)

// Error occurs here
name = "Alex Aldwin"
```

You'll likely end up declaring the vast majority of your variables as `const`.
Although the inability to reassign a variable declared as `const` initially
seems limiting, it allows us to circumnavigate a majority of the issues `var`
poses, especially when writing web applications, where consistent integrity
data and functions is critical.




### __Part Three:__ Let ###

What if we need to reassign a variable's initial value though? Certain code
constructs like the `for-loop` shown in our `var` example require variable
reassignment as an inherent mechanic. If ES6 syntax only offered us `const`,
we would then be forced to declare our variables using `var`, and practically
be back where we started. Thankfully, ES6 syntax JavaScript offers the `let`
declaration, which preserves the "block scope" accessibility of the `const`
declaration while allowing a programmer to reassign the value of the variable
declared.

This means that, for any variable declared using `let`, we may utilize
reassignment and short-hand reassignment operators. Then for the code below:

```
  for(let k=0; k<10; k+=1){
    console.log("Value of k is:",k)
  }

  k +=1
  console.log("The value of k is now:",k)
```

note that the variable `k`, declared using `let`, works perfectly within the
scope of the `for-loop` in which it operates. However, unlike our previous
`for-loop` where we declared the iterator using `var`, when we attempt to access
`k` outside the scope of the `for-loop`, JavaScript immediately throws an error,
informing us that `k` is undeclared (that outside the `for-loop`, JavaScript has
no knowledge of the variable `k`, such that for all intents and purposes `k`
does not exist outside that `for-loop`).
