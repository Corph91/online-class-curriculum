// VAR
for(var i=0; i<10; i+=1){
  console.log("Value of i",i)
}

i+=1
console.log("i is now:",i)

// for(let j=0; j<10; j+=1){
//   console.log("Value of j",j)
// }
//
// j+=1
// console.log("j is now:",j)




// CONST
const name = "John Jefferies"
const nameArray = []

console.log("My name is:",name)
nameArray.push(name)
console.log("I am the name array:",nameArray)

//Error occurs here
// name = "Alex Aldwin"

/* Challenge 1: write a function declared using ES6 syntax that takes an array
 * (also declared using ES6 syntax) and loops through said array, pushin numbers
 * 1-1000 (inclusive). Then, go through each number in the array and add one
 * to it, returning the result.
*/

const nums = []
const loopAdder = (numsIn) =>{
  for(let i=1; i<1001; i+=1){
    numsIn.push(i)
  }

  return numsIn.map((num) => num +=1)
}

console.log(loopAdder(nums))


/* Challenge 2: Write a function declared using ES6 syntax that returns an
 * object (declared using const) and returns that object with a name added to
 * it (tied to a co-requisite name property). The function should take the input
 * name as an argument.
 */

 const namer = (name) => {
   const nameObj = {}
   return nameObj['name'] = name
 }

 console.log(namer('Sean'))


 /* Challenge 3: Write a function declared using ES6 syntax that returns the
  * pluralized version of the input string. The function must assign the
  * input argument to a const variable.
  */

const pluralizer = (nameIn) => {
  const name = nameIn
  return nameIn + "s"
}

console.log(pluralizer("Coder"))


/* Challenge 4: Create two functions. The first should take an argument of
 * an array, sorting the array. The second function should be declared within
 * the first function, and return the median of the sorted array, removing all
 * duplicates.
 */

const numbers = [1,6,5,9,10,2,7,1,4,4,5,8,6]

 const sortMedian = (arrayIn) => {
   const array = arrayIn.sort((a,b) => (a > b) ? 1 : ((a < b) ? -1 : 0))
   const medianSingles = (arrayIn) => {
     const filtered = arrayIn.filter((num,index) => arrayIn.indexOf(num) === index)
     return (arrayIn.length%2 !== 0) ? (filtered[Math.floor(arrayIn.length/2)] + filtered[Math.ceil(arrayIn.length/2)])/2 : filtered[arrayIn.length/2]
   }

   return medianSingles(array)
 }

console.log(sortMedian(numbers))


/* Challenge 5: Create a function that takes a name as a greeting. If the name
 * passed in is your name, add it to a locally declared "greeting" variable
 * and return the result that returns "Hello <name>!". Else, the function should
 * return "I don't know you!". Log the function call to console.
 */

const pickyGreeter = (name) => {
  if(name === "Sean"){
    let greeting = "Hello " + name + "!"
    return greeting
  }
  return "I don't know you!"
}

console.log(pickyGreeter("Sean"))
console.log(pickyGreeter("David"))
