const http = require("http");
let port = 3000

http.createServer(function(request,response){
  // Send the HTTP header
  // HTTP Status: 200 : OK
  // Content Type: text/plain
  response.writeHead(200,{'Content-Type': 'text/plain'})

  // If everything checks out send response body "Hello humans!"
  response.end("Hello humans\n")
}).listen(port,() => console.log("Server running at:",port))
