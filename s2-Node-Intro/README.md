Sprint One

Introduction to Node

Core Topics:
- Intro To Node
- Mongo
- Postman
- Git/ Github

----

Recommended Pace

Session One
- Sprint plan and check in
- Overview of FS javascript and landscape
- Intro To Node: https://docs.google.com/presentation/d/1RgeLtO8DaSwroeRqQK1M25Y_w7dByuC-HG9Y8akRJoc/edit#slide=id.gc6fa3c898_0_0

Session Two
- Warmup: Customer constructor
- Node Code Along: https://github.com/fresh5447/node_intro_mis


Session Three
- Warmup
-


Session Four
- Restful API: Superheroes
  - Implement Hero Resource
  - Students pair and implement Villain Resource



Session Five
- Warmup
- Introduction To Flexbox
<!-- TODO Add Flexbox resource-->


Session Six
-
- Retrospecive
