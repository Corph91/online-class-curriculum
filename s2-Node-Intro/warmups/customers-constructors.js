var goldCustomers = ["Eric Sal", "Sarah Sizzle", "Steve Supl"];
var silverCustomers = ["Paul Rudd", "Sally Struthers", "Susan Sarandon"];
var platinumCustomers = ["Woug Dalter", "Myler Taxwell", "Sienna Scheid", "Tenn Jurner",
                          "Mody Celton", "Datie Kahlin", "Cean Sorbett"];

// our constructor should make an object that looks like so:
// {
  // first: "Woug",
  // last: "Dalter",
  // status: "platinum"
// }

// once you make your constructor function, loop through each array and build customer objects.
