const bballPlayers = [
   { first: 'Carlos', last: 'Beltran', homeruns: 421, retired: false },
   { first: 'Ken', last: 'Griffey Jr', homeruns: 630, retired: true },
   { first: 'Hank', last: 'Aaron', homeruns: 755, retired: true },
   { first: 'Ediwn', last: 'Encarnacion', homeruns: 310, retired: false }
   { first: 'Alex', last: 'Rodriguez', homeruns: 696, retired: true },
   { first: 'Willie', last: 'Mays', homeruns: 660, retired: true },
   { first: 'Jim', last: 'Thome', homeruns: 612, retired: true },
   { first: 'Sammy', last: 'Sosa', homeruns: 609, retired: true },
   { first: 'Babe', last: 'Ruth', homeruns: 714, retired: true },
   { first: 'Albert', last: 'Pujols', homeruns: 591, retired: false },
   { first: 'Barry', last: 'Bonds', homeruns: 762, retired: true },
   { first: 'Miguel', last: 'Cabrera', homeruns: 446, retired: false },
   { first: 'Ryan', last: 'Howard', homeruns: 382, retired: false }
 ];

 // Array.prototype.filter()
 // 1. Filter the list of players who have more then 500, but less than 600 home runs


 // Array.prototype.map()
 // 2. Give us an array of the players' first name and home runs


 // Array.prototype.sort()
 // 3. Sort the players by homeruns, most to least


 // Array.prototype.reduce()
 // 4. How many homeruns did all the players hit?

 // 5. How many homeruns do the active have?
