// TIP: Only release one-two challenges at a time.

// 1) create a file for todays JS challenges.
//
// throw a console.log in there and make sure it works!
// 2) Loop from 0 - 33 only print by 3s
//
// 3) A function that can take two random strings and combines them into a sentence. Bonus: Throw error if any arg is not a ‘String’
//
// 4) Make several object literals. Can be of anything you want (last week we did fruits for this challenge. Bonus: Give an object a method, that can tell you about itself
//
// 5) A function can take an array of numbers, and returns the sum (total) of all numbers in the Array. Bonus: Ignores values in the array if they are not of type Number. SUPER BONUS: The function will only accept an array. (Can not pass it a single number or string)
