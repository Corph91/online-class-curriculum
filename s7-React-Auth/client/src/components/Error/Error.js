import React from 'react';
import {Link} from 'react-router';

const Error = (props) => {
  return(
    <div className="container">
      <div className="col-md-12">
        <h1>Error:</h1>
        <h3>{props.error}</h3>
      </div>
      <div className="col-md-12">
        <Link className="btn btn-default" to={'/'}>Back to home!</Link>
      </div>
    </div>
  )
}

export default Error
