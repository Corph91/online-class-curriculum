import React from 'react';
import {Link} from 'react-router';

const ShowLogin = (props) => {
  return(
    <div>
      <div className="col-md-12">
        <h1>You are logged in!</h1>
      </div>
      <div className="col-md-12">
        <Link className="btn btn-default" to={`/user/${props.user_id}`}>User Page</Link>
      </div>
    </div>
  )
}

export default ShowLogin
