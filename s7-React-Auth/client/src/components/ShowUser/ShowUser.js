import React from 'react';
import {Link} from 'react-router';

const ShowUser = (props) => {
  return(
    <div>
      <div className="col-md-12">
        Hello from ShowUser!
      </div>
      <div className="col-md-12">
        <h1>User:</h1>
        <h3>{props.email}</h3>
      </div>
      <div className="col-md-12">
        <button type="button" className="btn btn-danger" onClick={(event) => props.logout(event)}>Logout</button>
        <Link className="btn btn-default" to={'/show'}>Back to Main Page</Link>
      </div>
    </div>
  )
}

export default ShowUser
