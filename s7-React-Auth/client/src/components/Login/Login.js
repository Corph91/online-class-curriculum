import React from 'react';
import {Link} from 'react-router';

const Login = (props) => {
  return(
    <div className="form-group">
      <div className="col-md-12">
        <h1>Log In</h1>
      </div>
      <div className="col-md-12">
        <label>email:</label>
        <input type="email" onChange={(event) => props.updateField('email', event.target.value)}/>
      </div>
      <div className="col-md-12">
        <label>password:</label>
        <input type="password" onChange={(event) => props.updateField('password', event.target.value)}/>
      </div>
      <div className="col-md-12">
        <button type="button" className="btn btn-primary" onClick={(event) => props.handleSubmit(event)}>Go!</button>
        <Link className="btn btn-default" to={'/signup'}>Sign Up</Link>
      </div>
    </div>
  )
}

export default Login
