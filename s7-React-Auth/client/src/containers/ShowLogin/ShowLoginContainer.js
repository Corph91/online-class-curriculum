import React,{Component} from 'react'
import $ from 'jquery';
import {ShowLogin} from '../../components';
import {browserHistory} from 'react-router';

class ShowLoginContainer extends Component{
  state = {
    valid: false,
    user_id: undefined
  }
  componentDidMount = () => this.getUser();
  getUser(){
    $.ajax({
      url: '/api/get_user',
      method: 'GET'
    }).done((response) => {
      (response._id && response._id.length > 0) ? this.setState({valid: true, user_id: response._id}) : browserHistory.push(`/error/${'You are not loggin in!'}`);
      console.log("User is logged in! ",response);
    })
  }
  render(){
    return(
      <div className="container">
      {(this.state.valid) ? <ShowLogin user_id={this.state.user_id}/> : "loading..."}
      </div>
    )
  }
}

export default ShowLoginContainer
