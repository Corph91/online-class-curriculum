import React,{Component} from 'react';
import {Error} from '../../components'

class ErrorContainer extends Component{
  state = {
    error: this.props.params.error,
  }
  render(){
    return(
      <Error error={this.state.error}/>
    )
  }

}

export default ErrorContainer
