import React,{Component} from 'react'
import $ from 'jquery';
import {ShowUser} from '../../components';
import {browserHistory} from 'react-router';

class ShowUserContainer extends Component{
  state = {
    valid: false,
    email: undefined,
    loaded: false
  }
  componentDidMount = () => this.getUser();
  getUser(){
    $.ajax({
      url: `/api/get_user`,
      method: 'GET'
    }).done((response) => {
      if(response._id !== this.props.params.user_id){
        browserHistory.push(`/error/${'Invalid access!'}`)
      }
      this.setState({valid: true, email: response.local.email, loaded: true});
       console.log("User matches! ",response._id);

    })
  }
  logout = this.logout.bind(this);
  logout(event){
    event.preventDefault();
    $.ajax({
      url: '/api/logout',
      method: 'GET'
    }).done((response) => response.loggedOut ? browserHistory.push('/login') : browserHistory.push(`/error/${'Error during logout!'}`));
  }
  render(){
    return(
      <div className="container">
      {(this.state.valid && this.state.loaded) ? <ShowUser email={this.state.email} logout={this.logout}/> : "loading..."}
      </div>
    )
  }
}

export default ShowUserContainer
